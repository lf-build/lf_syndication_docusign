﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Syndication.LFDocuSign;
using LendFoundry.Syndication.LFDocuSign.Models;
using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;
using LendFoundry.Syndication.LFDocuSign.TemplateConfiguration;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace LendFoundry.DocuSign.Persistence {
    public class DocusignReceipientRepository : MongoRepository<IReceipientDetails, ReceipientDetails>, IDocusignReceipientRepository {
        static DocusignReceipientRepository () {
            BsonClassMap.RegisterClassMap<ReceipientDetails> (map => {
                map.AutoMap ();
                var type = typeof (ReceipientDetails);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass (true);
            });
        }

        public DocusignReceipientRepository (LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration) : base (tenantService, configuration, "ReceipientDetails") {
            CreateIndexIfNotExists ("docusign_entityId", Builders<IReceipientDetails>.IndexKeys.Ascending (i => i.TenantId).Ascending (i => i.EntityId));
        }
        public async Task AddReceipientDetails (IReceipientDetails receipientDetails) {
            await Collection.InsertOneAsync (receipientDetails);
        }
        public async Task<IReceipientDetails> UpdateReceipientDetails (IReceipientDetails recipientDetails) {
                    var tenantId = TenantService.Current.Id;
            await Collection.UpdateOneAsync (Builders<IReceipientDetails>.Filter.Where (a => a.TenantId == tenantId &&
                    a.EntityId == recipientDetails.EntityId && a.RecipientId == recipientDetails.RecipientId && a.Name==recipientDetails.Name),
                Builders<IReceipientDetails>.Update.Set (a => a.Status, recipientDetails.Status)
                .Set (a => a.SendCount, recipientDetails.SendCount)
                .Set (a => a.Time, recipientDetails.Time)
                 .Set (a => a.SignedOn, recipientDetails.SignedOn)
            );
            return recipientDetails;

        }
        public async Task<IReceipientDetails> UpdateReceipientStatus (string entityId, string recipientId, Status status) {
            var tenantId = TenantService.Current.Id;
            await Collection.UpdateOneAsync (Builders<IReceipientDetails>.Filter.Where (a =>
                    a.EntityId == entityId && a.TenantId==tenantId && a.RecipientId == recipientId),
                Builders<IReceipientDetails>.Update.Set (a => a.Status, status)
            );
            return null;

        }

        public async Task<List<IReceipientDetails>> GetReceipientByEntityId (string entityId) {
            var tenantId = TenantService.Current.Id;
            return  await Collection.Find(Builders<IReceipientDetails>.Filter.Where (a =>a.TenantId==tenantId && a.EntityId == entityId)).ToListAsync();
        }
         
    }
}