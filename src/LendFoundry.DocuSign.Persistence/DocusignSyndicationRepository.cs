﻿
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Syndication.LFDocuSign;
using LendFoundry.Syndication.LFDocuSign.TemplateConfiguration;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using LendFoundry.DocumentManager;
using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;

namespace LendFoundry.DocuSign.Persistence
{
    public class DocusignSyndicationRepository : MongoRepository<IEnvelopeInfo, EnvelopeInfo>, IDocusignSyndicationRepository
    {
        static DocusignSyndicationRepository()
        {
            BsonClassMap.RegisterClassMap<EnvelopeInfo>(map =>
            {
                map.AutoMap();
                var type = typeof(EnvelopeInfo);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            }); 
            BsonClassMap.RegisterClassMap<ReceipientInfo>(map =>
            {
                map.AutoMap();
                var type = typeof(ReceipientInfo);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
            BsonClassMap.RegisterClassMap<Signer>(map =>
            {
                map.AutoMap();
                var type = typeof(DocuSignTab);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
        }

        public DocusignSyndicationRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "docusign-attributes")
        {
            CreateIndexIfNotExists("docusign_entityId", Builders<IEnvelopeInfo>.IndexKeys.Ascending(i => i.EntityId));
        }

        public async Task<List<IEnvelopeInfo>> GetEnvelopesInformation(string EntityId)
        {
            return await Query.Where(y => y.EntityId == EntityId).ToListAsync();
        }
        public async Task AddEnvelopeInformation(IEnvelopeInfo envelopeInfo)
        {
             await Collection.InsertOneAsync(envelopeInfo);
        }
        
    }
}