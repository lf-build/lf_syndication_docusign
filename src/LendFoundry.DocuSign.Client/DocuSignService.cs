﻿using LendFoundry.Syndication.LFDocuSign;
using LendFoundry.Syndication.LFDocuSign.Models;
using System.Threading.Tasks;
using LendFoundry.Syndication.LFDocuSign.Request;
using LendFoundry.Syndication.LFDocuSign.Response;
using System;
using System.Collections.Generic;
using RestSharp;
using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using System.Linq;

namespace LendFoundry.DocuSign.Client
{
    /// <summary>
    /// class used for injecting IBBBSearch service.
    /// </summary>
    public class DocuSignService : IDocuSignService
    {
        #region Public Constructors

        public DocuSignService(IServiceClient client)
        {
            Client = client;
        }
        #endregion Public Constructors

        #region Private Properties

        private IServiceClient Client { get; }
        #endregion Private Properties

        #region Public Methods
        public  Task<List<DownloadDocument>> DownloadDocuments(string entityType, string entityId, string envelopeId)
        {
            throw new NotImplementedException();
        }

        public async  Task<IEmbeddedUrlResponse> GenerateEmbeddedUrl(string entityType, string entityId, IGenerateEmbeddedUrlRequest requestParam)
        {
            IRestRequest request = new RestRequest("/{entitytype}/{entityid}/docusign/embeddedurlgenerate", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(requestParam);
            request.RequestFormat = DataFormat.Json;
            return await Client.ExecuteAsync<EmbeddedUrlResponse>(request);
        }

        public async Task<IGenerateEmbeddedViewResponse> GenerateEmbeddedView(string entityType, string entityId, string documentType, IEmbedSigningRequest requestParam)
        {
            IRestRequest request = new RestRequest("/{entitytype}/{entityid}/{documentType}/docusign/embeddedviewgenerate", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("documentType", documentType);
            request.AddJsonBody(requestParam);
            request.RequestFormat = DataFormat.Json;
            return await  Client.ExecuteAsync<GenerateEmbeddedViewResponse>(request);
        }

        public Task<EnvelopeAuditEventResponse> GetAuthenticationInfo(string entityType, string entityId, string envelopeId)
        {
            throw new NotImplementedException();
        }

        public Task<IGetEnvelopeInfoResponse> GetEnvelopeInfo(string entityType, string entityId, string envelopeId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> HasAuthenticated(string entityType, string entityId, string envelopeId)
        {
            throw new NotImplementedException();
        }

        public Task ProcessEvent(EventInfo eventInfo)
        {
            throw new NotImplementedException();
        }
        public async Task<dynamic> GetReceipientData(string receipientId, string entityId, string envelopeId)
        {
            IRestRequest request = new RestRequest("/{receipientId}/{entityid}/{envelopeId}/docusign/getenvelopedata", Method.GET);
            request.AddUrlSegment("receipientId", receipientId);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("envelopeId", envelopeId);
            return await Client.ExecuteAsync<dynamic>(request);
        }
        public async Task<List<IEnvelopeInfo>> GetEnvelopesByApplicationNumber(string entityId)
        {
            IRestRequest request = new RestRequest("/{entityid}/get/envelopes", Method.GET);
            request.AddUrlSegment("entityid", entityId);
            var EnvelopeInfoList = await Client.ExecuteAsync<List<EnvelopeInfo>>(request);
            return  EnvelopeInfoList.ToList<IEnvelopeInfo>();
        }
         public async Task<List<IReceipientDetails>> GetReceipientByEntityId(string entityId)
        {
            IRestRequest request = new RestRequest("/{entityid}/get/receipients", Method.GET);
            request.AddUrlSegment("entityid", entityId);
            var ReceipientList = await Client.ExecuteAsync<List<ReceipientDetails>>(request);
            return  ReceipientList.ToList<IReceipientDetails>();
        }
         public async  Task<IReceipientDetails> UpdateReceipientDetails(string entityType, string entityId, IReceipientDetails requestParam)
        {
            IRestRequest request = new RestRequest("/{entityId}/update/receipient", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(requestParam);
            request.RequestFormat = DataFormat.Json;
            return await Client.ExecuteAsync<ReceipientDetails>(request);
        }

        #endregion Public Methods
    }
}