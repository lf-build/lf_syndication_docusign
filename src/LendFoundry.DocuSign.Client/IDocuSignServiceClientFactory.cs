﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.LFDocuSign;

namespace LendFoundry.DocuSign.Client
{
    public interface IDocuSignServiceClientFactory
    {
        #region Public Methods

        IDocuSignService Create(ITokenReader reader);

        #endregion Public Methods
    }
}