﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.DocuSign.Client
{
    /// <summary>
    /// all Service injection
    /// </summary>
    public static class DocuSignServiceClientExtensions
    {
        #region Public Methods

        /// <summary>
        ///
        /// </summary>
        /// <param name="services">services</param>
        /// <param name="endpoint">endpoint of service</param>
        /// <param name="port">port of service</param>
        /// <returns>Service</returns>
       
        public static IServiceCollection AddDocuSignService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IDocuSignServiceClientFactory>(p => new DocuSignServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IDocuSignServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddDocuSignService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IDocuSignServiceClientFactory>(p => new DocuSignServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IDocuSignServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDocuSignService(this IServiceCollection services)
        {
            services.AddSingleton<IDocuSignServiceClientFactory>(p => new DocuSignServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IDocuSignServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        #endregion Public Methods
    }
}