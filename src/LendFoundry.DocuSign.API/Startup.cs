﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.DocuSign.Persistence;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.LFDocuSign;
using LendFoundry.Syndication.LFDocuSign.Proxy;
using LendFoundry.TemplateManager.Client;
using LendFoundry.Tenant.Client;
#if DOTNET2
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
#else
using LendFoundry.Foundation.Documentation;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.DocuSign.Api {
    /// <summary>
    /// 
    /// </summary>
    public class Startup {
        #region Public Methods
        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices (IServiceCollection services) {
            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2

            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("docs", new Info {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                        Title = "Tenant"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme () {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "LendFoundry.DocuSign.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();
#else
            services.AddSwaggerDocumentation ();
#endif
            // services
            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddTenantTime ();

            // aspnet mvc related
            services.AddMvc ().AddLendFoundryJsonOptions ();
            services.AddCors ();

            // interface implements
            services.AddConfigurationService<DocuSignConfiguration> (Settings.ServiceName);
            services.AddEventHub (Settings.ServiceName);
            services.AddTenantService ();
            services.AddDocumentManager ();
            services.AddTemplateManagerService ();
            services.AddDecisionEngine ();
            services.AddDependencyServiceUriResolver<DocuSignConfiguration> (Settings.ServiceName);
            services.AddLookupService ();
            services.AddMongoConfiguration (Settings.ServiceName);

            // internals

            services.AddTransient<IDocuSignConfiguration> (p => p.GetService<IConfigurationService<DocuSignConfiguration>> ().Get ());
            services.AddTransient<IDocuSignProxy, DocuSignProxy> ();
            services.AddTransient<IDocuSignService, DocuSignService> ();
            services.AddTransient<IDocusignServiceFactory, DocusignServiceFactory> ();
            services.AddTransient<IDocuSignListener, DocuSignListener> ();
            //services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));

            services.AddTransient<IDocusignSyndicationRepository, DocusignSyndicationRepository> ();
            services.AddTransient<IDocusignReceipientRepository, DocusignReceipientRepository> ();
            services.AddTransient<IDocusignReceipientRepositoryFactory, DocusignReceipientRepositoryFactory> ();
        }
        /// <summary>
        /// Configure
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure (IApplicationBuilder app, IHostingEnvironment env) {
            app.UseHealthCheck ();
            app.UseCors (env);

#if DOTNET2
            app.UseSwagger ();
            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/docs/swagger.json", "Syndication Docusign");
            });
#else
            app.UseSwaggerDocumentation ();
#endif
            app.UseErrorHandling ();
            app.UseRequestLogging ();
            app.UseDocuSignListener ();
            app.UseMvc ();
        }
        #endregion Public Methods
    }
}