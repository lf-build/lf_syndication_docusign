﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LFDocuSign;
using LendFoundry.Syndication.LFDocuSign.Request;
using System.Threading.Tasks;
using LendFoundry.Syndication.LFDocuSign.Response;
using LendFoundry.Syndication.LFDocuSign.Models;
using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.DocuSign.API.Controller
{
    /// <summary>
    /// 
    /// </summary>
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public ApiController(IDocuSignService service)
        {
            Service = service;
        }

        private IDocuSignService Service { get; set; }

        //GenerateEmbeddedView

        /// <summary>
        /// GenerateEmbeddedView
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="documentType"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/{documentType}/docusign/embeddedviewgenerate")]
        [ProducesResponseType(typeof(IGenerateEmbeddedViewResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GenerateEmbeddedView(string entityType, string entityId, string documentType, [FromBody]EmbedSigningRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.GenerateEmbeddedView(entityType, entityId, System.Net.WebUtility.UrlDecode(documentType), request));
            });
        }

        /// <summary>
        /// GenerateEmbeddedUrl
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/docusign/embeddedurlgenerate")]
        [ProducesResponseType(typeof(IEmbeddedUrlResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GenerateEmbeddedUrl(string entityType, string entityId, [FromBody]GenerateEmbeddedUrlRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.GenerateEmbeddedUrl(entityType, entityId, request));
            });
        }

        /// <summary>
        /// HasAuthenticated
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="envelopeId"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/docusign/hasauthenticated")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> HasAuthenticated(string entityType, string entityId, [FromBody]string envelopeId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.HasAuthenticated(entityType, entityId, envelopeId));
            });
        }

        /// <summary>
        /// GetAuthenticationInfo
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="envelopeId"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/docusign/getauthenticationinfo")]
        [ProducesResponseType(typeof(EnvelopeAuditEventResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAuthenticationInfo(string entityType, string entityId, [FromBody]string envelopeId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.GetAuthenticationInfo(entityType, entityId, envelopeId));
            });
        }

        /// <summary>
        /// GetEnvelopeInfo
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="envelopeId"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/docusign/getenvelopeinfo")]
        [ProducesResponseType(typeof(IGetEnvelopeInfoResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetEnvelopeInfo(string entityType, string entityId, [FromBody]string envelopeId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.GetEnvelopeInfo(entityType, entityId, envelopeId));
            });
        }

        /// <summary>
        /// DownloadDocuments
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="envelopeId"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/docusign/getenvelopeinfo")]
        [ProducesResponseType(typeof(DownloadDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DownloadDocuments(string entityType, string entityId, [FromBody]string envelopeId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.DownloadDocuments(entityType, entityId, envelopeId));
            });
        }

        /// <summary>
        /// GetReceipientData
        /// </summary>
        /// <param name="receipientId"></param>
        /// <param name="entityId"></param>
        /// <param name="envelopeId"></param>
        /// <returns></returns>
        [HttpGet("/{receipientId}/{entityid}/{envelopeId}/docusign/getenvelopedata")]
        // [ProducesResponseType(typeof(dynamic), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetReceipientData(string receipientId, string entityId, string envelopeId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.GetReceipientData(receipientId, entityId, envelopeId));
            });
        }

        /// <summary>
        /// GetEnvelopesByApplicationNumber
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("/{entityId}/get/envelopes")]
        [ProducesResponseType(typeof(IEnvelopeInfo), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetEnvelopesByApplicationNumber(string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.GetEnvelopesByApplicationNumber(entityId));
            });
        }

        /// <summary>
        /// GetReceipientByEntityId
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("/{entityId}/get/receipients")]
        [ProducesResponseType(typeof(IReceipientDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetReceipientByEntityId(string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.GetReceipientByEntityId(entityId));
            });
        }
        
        /// <summary>
        /// UpdateReceipientDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="receipientDetailRequest"></param>
        /// <returns></returns>
        [HttpPost("/{entityId}/update/receipient")]
        [ProducesResponseType(typeof(ReceipientDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateReceipientDetails(string entityType, string entityId, [FromBody]ReceipientDetails receipientDetailRequest)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Service.UpdateReceipientDetails(entityType, entityId, receipientDetailRequest));
            });
        }
       
    }
}