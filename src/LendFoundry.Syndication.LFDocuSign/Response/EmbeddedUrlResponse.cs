﻿namespace LendFoundry.Syndication.LFDocuSign.Response
{
    public class EmbeddedUrlResponse : IEmbeddedUrlResponse
    {
        public EmbeddedUrlResponse(Proxy.Response.IGenerateEmbeddedUrlResponse generateEmbeddedUrlResponse)
        {
            Url = generateEmbeddedUrlResponse?.Url;
        }
        public string Url { get; set; }
    }
}
