﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LFDocuSign.Models;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Response
{
    public class GenerateEmbeddedViewResponse: IGenerateEmbeddedViewResponse
    {
        public GenerateEmbeddedViewResponse()
        {
        }
        public GenerateEmbeddedViewResponse(Proxy.Response.IGenerateEmbeddedViewResponse response)
        {
            EnvelopeSummary = new EnvelopeSummary(response?.EnvelopeSummary);
        }
        [JsonConverter(typeof(InterfaceConverter<IEnvelopeSummary,EnvelopeSummary>))]
        public IEnvelopeSummary EnvelopeSummary { get; set; }
        public string[] RecepientIds { get; set; }
    }
}
