﻿using LendFoundry.Syndication.LFDocuSign.Models;

namespace LendFoundry.Syndication.LFDocuSign.Response
{
    public class GetEnvelopeInfoResponse: IGetEnvelopeInfoResponse
    {
        public GetEnvelopeInfoResponse()
        {

        }
        public GetEnvelopeInfoResponse(Proxy.Response.IGetEnvelopeInfoResponse response)
        {
            Envelope =new Envelope(response?.Envelope);
        }
        public IEnvelope Envelope { get; set; }
    }
}
