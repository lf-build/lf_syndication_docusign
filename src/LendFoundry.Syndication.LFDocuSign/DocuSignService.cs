﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LFDocuSign.Events;
using LendFoundry.Syndication.LFDocuSign.Models;
using LendFoundry.Syndication.LFDocuSign.Proxy;
using LendFoundry.Syndication.LFDocuSign.Proxy.Models;
using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;
using LendFoundry.Syndication.LFDocuSign.Request;
using LendFoundry.Syndication.LFDocuSign.Response;
using LendFoundry.Syndication.LFDocuSign.TemplateConfiguration;
using LendFoundry.TemplateManager;
using LendFoundry.Security.Tokens;
using Newtonsoft.Json;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.Syndication.LFDocuSign
{
    public class DocuSignService : IDocuSignService
    {
        public DocuSignService(IDocuSignProxy proxy,
            ILookupService lookUp,
            IEventHubClient eventHub,
            IDocumentManagerService documentManagerService,
            ITemplateManagerService templateManagerService,
            IDocuSignConfiguration configuration,
            IDecisionEngineService decisionEngineService,
            ILogger logger,
            ITenantTime tenantTime,
            IDocusignSyndicationRepository docusignRepository,
            IDocusignReceipientRepository docusignReceipientRepository)
        {
            if (proxy == null)
                throw new ArgumentException(nameof(proxy));
            if (lookUp == null)
                throw new ArgumentException(nameof(lookUp));
            if (eventHub == null)
                throw new ArgumentException(nameof(eventHub));
            if (documentManagerService == null)
                throw new ArgumentException(nameof(documentManagerService));
            if (templateManagerService == null)
                throw new ArgumentException(nameof(templateManagerService));
            if (docusignRepository == null)
                throw new ArgumentException(nameof(docusignRepository));
            if (docusignReceipientRepository == null)
                throw new ArgumentException(nameof(docusignReceipientRepository));
            DocuSignProxy = proxy;
            Lookup = lookUp;
            EventHub = eventHub;
            DocumentManagerService = documentManagerService;
            TemplateManagerService = templateManagerService;
            Logger = logger;
            TenantTime = tenantTime;
            DocusignRepository = docusignRepository;
            DocusignReceipientRepository = docusignReceipientRepository;
        }
        public DocuSignService(
            IEventHubClient eventHub,
            ILogger logger,
            IDecisionEngineService decisionEngineService,
            IDocuSignConfiguration configuration,
            IDocusignReceipientRepository docusignReceipientRepository)
        {
            if (eventHub == null)
                throw new ArgumentException(nameof(eventHub));
            if (decisionEngineService == null)
                throw new ArgumentException(nameof(decisionEngineService));
            if (configuration == null)
                throw new ArgumentException(nameof(Configuration));
            if (logger == null)
                throw new ArgumentException(nameof(logger));
            if (docusignReceipientRepository == null)
                throw new ArgumentException(nameof(docusignReceipientRepository));
            EventHub = eventHub;
            DecisionEngineService = decisionEngineService;
            Configuration = configuration;
            Logger = logger;
            DocusignReceipientRepository = docusignReceipientRepository;
        }
        #region Private Members
        private IDocuSignProxy DocuSignProxy { get; set; }
        private ILookupService Lookup { get; set; }
        private IEventHubClient EventHub { get; set; }
        private IDocumentManagerService DocumentManagerService { get; set; }
        private ITemplateManagerService TemplateManagerService { get; set; }
        private IDecisionEngineService DecisionEngineService { get; set; }
        private IDocuSignConfiguration Configuration { get; set; }
        private ILogger Logger { get; set; }
        private ITenantTime TenantTime { get; set; }
        private IDocusignSyndicationRepository DocusignRepository { get; set; }
        private IDocusignReceipientRepository DocusignReceipientRepository { get; set; }
        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
        private async Task<IDocuSignTemplate> GetDocusignConfigurationForTemplate(string templatename, string version)
        {
            if (string.IsNullOrWhiteSpace(templatename))
                throw new ArgumentException(nameof(templatename));
            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException(nameof(version));
            object genricObj = null;
            IDocuSignTemplate docusignConfig = null;
            try
            {
                var template = await TemplateManagerService.Get(templatename, version, Format.Html);
                if (template == null)
                    throw new NotFoundException($"Template not found");
                template.Properties?.TryGetValue("signature", out genricObj);
                if (genricObj != null)
                {
                    string jsonString = JsonConvert.SerializeObject(genricObj);
                    docusignConfig = JsonConvert.DeserializeObject<DocuSignTemplate>(jsonString);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return docusignConfig;
        }
        private async Task<ITemplateInfo> GetTemplateDetailsForDocument(string entityType, string entityId, string documentId)
        {
            DocumentManager.IDocument document = null;
            try
            {
                if (string.IsNullOrWhiteSpace(documentId))
                    throw new ArgumentException(nameof(documentId));
                document = await DocumentManagerService.Get(entityType, entityId, documentId);
                if (document == null)
                    throw new NotFoundException($"No document found for documentId {documentId}");
            }
            catch (Exception)
            {
                throw;
            }
            //string jsonString = JsonConvert.SerializeObject(document.Metadata);
           return JsonConvert.DeserializeObject<TemplateInfo> (document.Metadata.ToString ());
        }
        private async Task<byte[]> GetDocumentBytes(string entityType, string entityId, string documentId)
        {
            byte[] documentBytes = null;
            try
            {

                if (string.IsNullOrWhiteSpace(documentId))
                    throw new ArgumentException(nameof(documentId));
                Stream documentStream = await DocumentManagerService.Download(entityType, entityId, documentId);
                if (documentStream == null)
                    throw new NotFoundException($"No document found for documentId {documentId}");
                using (var streamReader = new StreamReader(documentStream))
                {
                    using (var memStream = new MemoryStream())
                    {
                        streamReader.BaseStream.CopyTo(memStream);
                        documentBytes = memStream.ToArray();
                    }
                }

                #region Get the document to be signed
                #endregion

            }
            catch (Exception)
            {
                throw;
            }
            return documentBytes;
        }
        #endregion

        #region public Member

        public async Task<IGenerateEmbeddedViewResponse> GenerateEmbeddedView(string entityType, string entityId, string documentType, IEmbedSigningRequest request)
        {

            #region Validation
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (request.DocumentId.Count == 0)
                throw new ArgumentNullException(nameof(request.DocumentId));
            #endregion            

            #region Request.Receipients
            if (request.Receipients == null || request.Receipients.Count == 0)
                throw new ArgumentNullException(nameof(request.Receipients));

            foreach (var receipient in request.Receipients)
            {
                if (string.IsNullOrWhiteSpace(receipient.Email))
                    throw new ArgumentException(nameof(receipient.Email));

                if (string.IsNullOrWhiteSpace(receipient.Name))
                    throw new ArgumentException(nameof(receipient.Name));
            }
            #endregion

            try
            {
                Logger.Info("Started Execution for GenerateEmbeddedView Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Syndication: DocuSign");

                var requestObj = new Proxy.Request.EmbedSigningRequest(request);
                if (request.DocumentId.Count == 1)
                {
                    var tempaleInfo = await GetTemplateDetailsForDocument(entityType, entityId, request.DocumentId[0]);
                    requestObj.TemplateDocuSignConfig = await GetDocusignConfigurationForTemplate(tempaleInfo.TemplateName, tempaleInfo.TemplateVersion);
                }
                else
                    requestObj.TemplateDocuSignConfig = null;

                requestObj.File = new List<IDocumentFile>();
                foreach (var documentId in request.DocumentId)
                {
                    var tempaleInfo = await GetTemplateDetailsForDocument(entityType, entityId, documentId);
                    var fileByte = await GetDocumentBytes(entityType, entityId, documentId);
                    requestObj.File.Add(new DocumentFile()
                    {
                        DocumentId = documentId,
                        FileBtyes = fileByte,
                        DocumentName = tempaleInfo.TemplateName
                    });
                }
                var recipients = await DocusignReceipientRepository.GetReceipientByEntityId(entityId);
                if (recipients == null || recipients.Count==0)
                {
                    foreach (var receipient in request.Receipients)
                    {
                        ReceipientDetails receipientsDetail = new ReceipientDetails()
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            RecipientId = receipient.RecipientId,
                            Name = receipient.Name,
                            Email = receipient.Email,
                            Status = Status.None,
                            SendCount = 0,
                            Time = new TimeBucket(TenantTime.Now)
                        };
                        DocusignReceipientRepository.Add(receipientsDetail);
                    }
                }
                var response = await Task.Run(() => DocuSignProxy.GenerateEmbeddedView(entityId, requestObj, documentType));
                var result = new GenerateEmbeddedViewResponse(response);
                result.RecepientIds = response.RecepientIds;
                await EventHub.Publish(new EmbeddedViewGenerated
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = request,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                Logger.Info("Completed Execution for GenerateEmbeddedView Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Syndication: DocuSign");

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("Error While Processing GenerateEmbeddedView Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Syndication: DocuSign" + "Exception" + ex.Message);

                await EventHub.Publish(new EmbeddedViewGenerateFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = null
                });
                throw;
            }
        }

        public async Task<IEmbeddedUrlResponse> GenerateEmbeddedUrl(string entityType, string entityId, IGenerateEmbeddedUrlRequest request)
        {
            #region Validation
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (string.IsNullOrWhiteSpace(request.ClientUserId))
                throw new ArgumentNullException(nameof(request.ClientUserId));

            if (string.IsNullOrWhiteSpace(request.EnvelopeId))
                throw new ArgumentNullException(nameof(request.EnvelopeId));

            if (string.IsNullOrWhiteSpace(request.SignerEmail))
                throw new ArgumentNullException(nameof(request.SignerEmail));

            if (string.IsNullOrWhiteSpace(request.SignerName))
                throw new ArgumentNullException(nameof(request.SignerName));
            #endregion

            var requestObj = new Proxy.Request.GenerateEmbeddedUrlProxyRequest
            {
                ClientUserId = request.ClientUserId,
                EnvelopeId = request.EnvelopeId,
                ReturnUrl = request.ReturnUrl,
                SignerEmail = request.SignerEmail,
                SignerName = request.SignerName,
                RecipientId = request.RecipientId
            };

            var result = await Task.Run(() => DocuSignProxy.GenerateEmbeddedUrl(requestObj));
            return new EmbeddedUrlResponse(result);
        }

        public async Task<bool> HasAuthenticated(string entityType, string entityId, string envelopeId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (string.IsNullOrWhiteSpace(envelopeId))
                throw new ArgumentNullException(nameof(envelopeId));

            return await Task.Run(() => DocuSignProxy.HasAuthenticated(envelopeId));
        }

        public async Task<EnvelopeAuditEventResponse> GetAuthenticationInfo(string entityType, string entityId, string envelopeId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (string.IsNullOrWhiteSpace(envelopeId))
                throw new ArgumentNullException(nameof(envelopeId));

            return await Task.Run(() => DocuSignProxy.GetAuthenticationInfo(envelopeId));
        }

        public async Task<IGetEnvelopeInfoResponse> GetEnvelopeInfo(string entityType, string entityId, string envelopeId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (string.IsNullOrWhiteSpace(envelopeId))
                throw new ArgumentException(nameof(envelopeId));

            var response = await Task.Run(() => DocuSignProxy.GetEnvelopeInfo(envelopeId));
            var result = new GetEnvelopeInfoResponse(response);
            return result;
        }

        public async Task<List<DownloadDocument>> DownloadDocuments(string entityType, string entityId, string envelopeId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (string.IsNullOrWhiteSpace(envelopeId))
                throw new ArgumentException(nameof(envelopeId));
            var result = await Task.Run(() => DocuSignProxy.DownloadDocuments(envelopeId));
            return new List<DownloadDocument>();
        }
        public async Task<dynamic> GetReceipientData(string receipientId, string entityId, string envelopeId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (string.IsNullOrWhiteSpace(envelopeId))
                throw new ArgumentException(nameof(envelopeId));
            var result = await Task.Run(() => DocuSignProxy.GetReceipientData(envelopeId, receipientId));
            return result;
        }

        public async Task<List<IEnvelopeInfo>> GetEnvelopesByApplicationNumber(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var result = await Task.Run(() => DocusignRepository.GetEnvelopesInformation(entityId));
            return result;
        }
        public async Task<List<IReceipientDetails>> GetReceipientByEntityId(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var result = await Task.Run(() => DocusignReceipientRepository.GetReceipientByEntityId(entityId));
            return result;
        }
        public async Task<IReceipientDetails> UpdateReceipientDetails(string entityType, string entityId, IReceipientDetails receipientDetailRequest)
        {


            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (receipientDetailRequest == null)
                throw new ArgumentNullException(nameof(receipientDetailRequest));

            var recipients = await DocusignReceipientRepository.GetReceipientByEntityId(entityId);
            var recipient = recipients.Where(r => r.RecipientId == receipientDetailRequest.RecipientId).FirstOrDefault();
            if (recipient == null)
                throw new ArgumentException($"No recipient with this {receipientDetailRequest.RecipientId} found");

            receipientDetailRequest.SignedOn = receipientDetailRequest.SignedOn == null ? recipient.SignedOn : receipientDetailRequest.SignedOn;
            receipientDetailRequest.SendCount = receipientDetailRequest.SendCount != 0 ? receipientDetailRequest.SendCount : recipient.SendCount;
            receipientDetailRequest.Time = receipientDetailRequest.Time != null ? receipientDetailRequest.Time : recipient.Time;
            var result = await Task.Run(() => DocusignReceipientRepository.UpdateReceipientDetails(receipientDetailRequest));
            return result;
        }
        public async Task ProcessEvent(EventInfo eventInfo)
        {
            var eventConfiguration = Configuration.events.Where(e => e.Name == eventInfo.Name)?.FirstOrDefault();
            string responseData = eventConfiguration.Response.FormatWith(eventInfo);
            dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);
            Logger.Info($"Starting rule {eventConfiguration.Rule} execution...");
            var ruleResult = DecisionEngineService.Execute<dynamic, DocusignEventRequest>(eventConfiguration.Rule, new { input = data });
            Logger.Info($"Completed rule {eventConfiguration.Rule} execution...");
            if (ruleResult != null)
            {
                if (ruleResult.Status == DocusignEventsType.Completed.ToString())
                {
                    var FileByteList = new List<byte[]>();
                    foreach (var bytes in ruleResult.DocumentBytes)
                    {
                        byte[] fileByte = Convert.FromBase64String(bytes);
                        FileByteList.Add(fileByte);
                    };
                    if (eventConfiguration.CompletionEvents != null && eventConfiguration.CompletionEvents.Count > 0)
                    {
                        foreach (var eventname in eventConfiguration.CompletionEvents)
                        {
                            await EventHub.Publish(eventname, new
                            {
                                EntityId = ruleResult?.EntityId,
                                EntityType = ruleResult?.EntityType,
                                Response = new { DocumentContent = FileByteList, DocumentName = ruleResult.DocumentName },
                                Request = ruleResult?.EntityId,
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            Logger.Info($"Event #{eventname} published for #{ruleResult.EntityId}");
                        }
                        var result = DecisionEngineService.Execute<dynamic, DocusignEventRequest>(eventConfiguration.ReceipientRule, new { input = data });
                        if (result != null)
                        {
                            foreach (var receipient in result.ReceipientStatus)
                            {
                                IReceipientDetails receipientsDetail = new ReceipientDetails()
                                {
                                    EntityId = ruleResult?.EntityId,
                                    EntityType = ruleResult?.EntityType,
                                    RecipientId = receipient.RecipientId,
                                    Name = receipient.Name,
                                    Email = receipient.Email,
                                    Status = Status.Completed
                                };
                                await UpdateReceipientDetails(ruleResult.EntityType, ruleResult.EntityId, receipientsDetail);
                            }
                        }

                    }
                    else
                    {
                        Logger.Info($"No event found to be publish for event {eventConfiguration.Name}");
                        Logger.Info($"Starting rule {eventConfiguration.ReceipientRule} execution...");

                    }
                }
                else
                {
                    Logger.Info($"Docusign status is {ruleResult.Status}");
                    var result = DecisionEngineService.Execute<dynamic, DocusignEventRequest>(eventConfiguration.ReceipientRule, new { input = data });
                    if (result != null)
                    {
                        if (string.IsNullOrWhiteSpace(result.EntityId))
                        {
                            Logger.Error($"EntityId not available in rule: {eventConfiguration.ReceipientRule} for payload ", null, data);
                            return;
                        }
                        var status = Status.None;
                        if (result.ReceipientStatus != null)
                        {
                            foreach (var receipient in result.ReceipientStatus)
                            {
                                status = receipient.Status.ToLower() == "completed" ? status = Status.Completed : status = Status.Sent;
                                IReceipientDetails receipientsDetail = new ReceipientDetails()
                                {
                                    EntityId = result?.EntityId,
                                    EntityType = result?.EntityType,
                                    RecipientId = receipient.RecipientId,
                                    Name = receipient?.Name,
                                    Email = receipient?.Email,
                                    Status = status,
                                    SignedOn = new TimeBucket(Convert.ToDateTime(receipient?.SignedOn))
                                };
                                await UpdateReceipientDetails(result.EntityType, result.EntityId, receipientsDetail);
                            }
                        }
                    }
                }
            }
            else
            {
                Logger.Info($"rule result for rule {eventConfiguration.Rule} is {ruleResult}");
            }

        }
        #endregion
    }
}
