﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LendFoundry.Foundation.Date;
using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;
using LendFoundry.Syndication.LFDocuSign.Proxy.Request;
using LendFoundry.Syndication.LFDocuSign.Proxy.Response;
using LendFoundry.Syndication.LFDocuSign.TemplateConfiguration;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using RestSharp;

namespace LendFoundry.Syndication.LFDocuSign.Proxy {
    public class DocuSignProxy : IDocuSignProxy {
        public DocuSignProxy (IDocuSignConfiguration configuration, ITenantService tenantService, IDocusignSyndicationRepository docusignRepository) {
            if (configuration == null)
                throw new ArgumentException (nameof (configuration));
            if (tenantService == null)
                throw new ArgumentException (nameof (tenantService));
            Configuration = configuration;
            TenantService = tenantService;
            DocusignRepository = docusignRepository;
        }
        #region Properties
        private IDocuSignConfiguration Configuration { get; set; }
        private ITenantService TenantService { get; set; }
        private string AccountId { get; set; }
        private string BaseUrl { get; set; }
        private IDocusignSyndicationRepository DocusignRepository { get; set; }
        #endregion

        #region Private Methods
        private void DocuSignLogin () {
            RestClient client = new RestClient (Configuration.Url);
            IRestRequest req = new RestRequest ("/v2/login_information", Method.GET);
            LoginInformation LoginInfo = ExecuteRequest<LoginInformation> (client, req);
            ExtractData (LoginInfo);
        }
        private void ExtractData (LoginInformation LoginInfo) {
            foreach (LoginAccount loginAcct in LoginInfo.LoginAccounts) {
                if (loginAcct.IsDefault == "true") {
                    AccountId = loginAcct.AccountId;
                    BaseUrl = loginAcct.BaseUrl;
                    break;
                }
            }

            if (AccountId == null) {
                AccountId = LoginInfo.LoginAccounts[0].AccountId;
                BaseUrl = LoginInfo.LoginAccounts[0].BaseUrl;
            }
        }
        private Tabs CreateTabsForTemplate (ISignerConfiguration signerConfiguration, string documentId, string recipientId) {
            Tabs tempTabs = new Tabs ();
            foreach (var tab in signerConfiguration.Tabs) {
                AddTab (ref tempTabs, tab.TabType, tab.Configuration, documentId, recipientId);
            }
            return tempTabs;
        }
        private void AddTab (ref Tabs tab, TabTypes tabtype, ITabConfiguration tabConfiguration, string documentId, string recipientId) {

            switch (tabtype) {
                case TabTypes.DateSigned:
                    if (tab.DateSignedTabs == null || (tab.DateSignedTabs != null && tab.DateSignedTabs.Count == 0)) {
                        tab.DateSignedTabs = new List<DateSigned> ();
                    }
                    DateSigned dateSigned = new DateSigned ();
                    dateSigned.DocumentId = documentId;
                    dateSigned.TabLabel = tabConfiguration.AnchorString;
                    if (string.IsNullOrWhiteSpace (tabConfiguration.AnchorString)) {
                        dateSigned.XPosition = tabConfiguration.XPosition.ToString ();
                        dateSigned.YPosition = tabConfiguration.YPosition.ToString ();
                    } else {
                        dateSigned.AnchorCaseSensitive = tabConfiguration.AnchorCaseSensitive.ToString ();
                        dateSigned.AnchorMatchWholeWord = tabConfiguration.AnchorMatchWholeWord.ToString ();
                        dateSigned.AnchorString = tabConfiguration.AnchorString;
                    }
                    dateSigned.PageNumber = tabConfiguration.PageNumber.ToString ();
                    dateSigned.RecipientId = recipientId;
                    tab.DateSignedTabs.Add (dateSigned);
                    break;
                case TabTypes.Decline:
                    break;
                case TabTypes.FirstName:
                    break;
                case TabTypes.InitialHere:
                    if (tab.InitialHereTabs == null || (tab.InitialHereTabs != null && tab.InitialHereTabs.Count == 0)) {
                        tab.InitialHereTabs = new List<InitialHere> ();
                    }
                    InitialHere initialHere = new InitialHere ();
                    initialHere.DocumentId = documentId;
                    initialHere.TabLabel = tabConfiguration.AnchorString;
                    if (string.IsNullOrWhiteSpace (tabConfiguration.AnchorString)) {
                        initialHere.XPosition = tabConfiguration.XPosition.ToString ();
                        initialHere.YPosition = tabConfiguration.YPosition.ToString ();
                    } else {
                        initialHere.AnchorCaseSensitive = tabConfiguration.AnchorCaseSensitive.ToString ();
                        initialHere.AnchorMatchWholeWord = tabConfiguration.AnchorMatchWholeWord.ToString ();
                        initialHere.AnchorString = tabConfiguration.AnchorString;
                    }
                    initialHere.PageNumber = tabConfiguration.PageNumber.ToString ();
                    initialHere.RecipientId = recipientId;
                    tab.InitialHereTabs.Add (initialHere);
                    break;
                case TabTypes.LastName:
                    break;
                case TabTypes.SignHere:
                    if (tab.SignHereTabs == null || (tab.SignHereTabs != null && tab.SignHereTabs.Count == 0)) {
                        tab.SignHereTabs = new List<SignHere> ();
                    }
                    SignHere signHere = new SignHere ();
                    signHere.DocumentId = documentId;
                    signHere.TabLabel = tabConfiguration.AnchorString;
                    if (string.IsNullOrWhiteSpace (tabConfiguration.AnchorString)) {
                        signHere.XPosition = tabConfiguration.XPosition.ToString ();
                        signHere.YPosition = tabConfiguration.YPosition.ToString ();
                    } else {
                        signHere.AnchorCaseSensitive = tabConfiguration.AnchorCaseSensitive.ToString ();
                        signHere.AnchorMatchWholeWord = tabConfiguration.AnchorMatchWholeWord.ToString ();
                        signHere.AnchorString = tabConfiguration.AnchorString;
                    }
                    signHere.PageNumber = tabConfiguration.PageNumber.ToString ();
                    signHere.RecipientId = recipientId;
                    tab.SignHereTabs.Add (signHere);
                    break;
                case TabTypes.EmailTab:
                    tab.EmailTabs = new List<Email> ();
                    Email email = new Email ();
                    email.DocumentId = documentId;
                    if (string.IsNullOrWhiteSpace (tabConfiguration.AnchorString)) {
                        email.XPosition = tabConfiguration.XPosition.ToString ();
                        email.YPosition = tabConfiguration.YPosition.ToString ();
                    } else {
                        email.AnchorCaseSensitive = tabConfiguration.AnchorCaseSensitive.ToString ();
                        email.AnchorMatchWholeWord = tabConfiguration.AnchorMatchWholeWord.ToString ();
                        email.AnchorString = tabConfiguration.AnchorString;
                    }
                    email.PageNumber = tabConfiguration.PageNumber.ToString ();
                    email.RecipientId = recipientId;
                    tab.EmailTabs.Add (email);
                    break;
                case TabTypes.SsnTab:
                    if (tab.SsnTabs == null || (tab.SsnTabs != null && tab.SsnTabs.Count == 0)) {
                        tab.SsnTabs = new List<Ssn> ();
                    }
                    Ssn SSN = new Ssn ();
                    SSN.Name = tabConfiguration.Name;
                    SSN.TabLabel = tabConfiguration.AnchorString;
                    SSN.DocumentId = documentId;
                    SSN.TabId = tabConfiguration.AnchorString;
                    SSN.PageNumber = tabConfiguration.PageNumber.ToString ();
                    SSN.RecipientId = recipientId;
                    SSN.Locked = "false";
                    SSN.Required = tabConfiguration.Required.ToString ();
                    if (string.IsNullOrWhiteSpace (tabConfiguration.AnchorString)) {
                        SSN.XPosition = tabConfiguration.XPosition.ToString ();
                        SSN.YPosition = tabConfiguration.YPosition.ToString ();
                    } else {
                        SSN.AnchorCaseSensitive = tabConfiguration.AnchorCaseSensitive.ToString ();
                        SSN.AnchorMatchWholeWord = tabConfiguration.AnchorMatchWholeWord.ToString ();
                        SSN.AnchorString = tabConfiguration.AnchorString;
                    }
                    tab.SsnTabs.Add (SSN);
                    break;
                case TabTypes.TextTab:
                    if (tab.TextTabs == null || (tab.TextTabs != null && tab.TextTabs.Count == 0)) {
                        tab.TextTabs = new List<Text> ();
                    }
                    Text text = new Text ();
                    text.Name = tabConfiguration.Name;
                    text.TabLabel = tabConfiguration.AnchorString;
                    text.DocumentId = documentId;
                    text.PageNumber = tabConfiguration.PageNumber.ToString ();
                    text.RecipientId = recipientId;
                    text.Locked = "false";
                    text.Required = tabConfiguration.Required.ToString ();
                    if (string.IsNullOrWhiteSpace (tabConfiguration.AnchorString)) {
                        text.XPosition = tabConfiguration.XPosition.ToString ();
                        text.YPosition = tabConfiguration.YPosition.ToString ();
                    } else {
                        text.AnchorCaseSensitive = tabConfiguration.AnchorCaseSensitive.ToString ();
                        text.AnchorMatchWholeWord = tabConfiguration.AnchorMatchWholeWord.ToString ();
                        text.AnchorString = tabConfiguration.AnchorString;
                    }
                    tab.TextTabs.Add (text);
                    break;
                case TabTypes.ZipTab:
                    break;

            }
        }
        private EnvelopeDefinition CreateEnvelope (string entityId, IEmbedSigningRequest embedSigningRequest) {
            #region create Envelope Defination
            Random numberGenerator = new Random ();
            EnvelopeDefinition envDef = new EnvelopeDefinition ();
            envDef.Status = "sent";
            #region customField for entityId
            envDef.CustomFields = new CustomFields () {
                TextCustomFields = new List<TextCustomField> () {
                new TextCustomField () {
                FieldId = entityId,
                Name = "ApplicationNumber",
                Show = "false",
                Value = entityId,
                Required = "false"
                }
                }
            };
            #endregion
            #endregion
            #region  Add a document to the envelope
            envDef.Documents = new List<Document> ();
            foreach (var request in embedSigningRequest.File) {
                string documentId = numberGenerator.Next ().ToString ();
                Document doc = new Document ();
                doc.DocumentBase64 = System.Convert.ToBase64String (request.FileBtyes);
                doc.Name = request.DocumentName;
                doc.DocumentId = documentId;
                envDef.Documents.Add (doc);

            }

            #endregion
            envDef.Recipients = new Recipients ();
            envDef.Recipients.Signers = new List<Signer> ();

            #region Add a CC receipient Email
            int ccReceipintIndex = 0;
            if (embedSigningRequest.CCReceipients != null && embedSigningRequest.CCReceipients.Count > 0) {
                envDef.Recipients.CarbonCopies = new List<CarbonCopy> ();
                foreach (var ccreceipient in embedSigningRequest.CCReceipients) {
                    // Add a cc recipient 
                    ccReceipintIndex++;
                    CarbonCopy cc = new CarbonCopy ();
                    cc.Email = ccreceipient.Email;
                    cc.Name = ccreceipient.Name;
                    cc.RecipientId = numberGenerator.Next ().ToString ();
                    cc.RoutingOrder = ccReceipintIndex.ToString ();
                    envDef.Recipients.CarbonCopies.Add (cc);
                }
            }
            #endregion
            #region Add a receipient Email
            int receipintIndex = 0;
            foreach (var receipient in embedSigningRequest.Receipients) {
                // Add a recipient to sign the document
                var signerConfiguration = embedSigningRequest.TemplateDocuSignConfig.SignerConfiguration.FirstOrDefault (x => x.RoutingOrder == receipient.RoutingNumber);
                if (signerConfiguration == null)
                    throw new ArgumentNullException (nameof (signerConfiguration));
                Signer signer = new Signer ();
                if (signerConfiguration.RoutingOrder != null && signerConfiguration.RoutingOrder != 0) {
                    var routingOrder = ccReceipintIndex + signerConfiguration.RoutingOrder;
                    signer.RoutingOrder = routingOrder.ToString ();
                }
                signer.Email = receipient.Email;
                signer.Name = receipient.Name;
                signer.RecipientId = signerConfiguration.RecipientId;
                signer.CustomFields = new List<string> () {
                    receipient?.RecipientId
                };

                signer.Tabs = CreateTabsForTemplate (signerConfiguration, embedSigningRequest.File[0].DocumentId, signer.RecipientId);
                signer.ClientUserId = receipient.ClientId;
                envDef.Recipients.Signers.Add (signer);

                receipintIndex++;

            }
            #endregion
            envDef.EmailBlurb = embedSigningRequest?.EmailBody;
            ConfigureEvelope (ref envDef, embedSigningRequest.TemplateDocuSignConfig);
            return envDef;
        }
        private ViewUrl GetViewUrl (IGenerateEmbeddedUrlProxyRequest request) {
            RecipientViewRequest viewOptions = new RecipientViewRequest () {
                ReturnUrl = request.ReturnUrl,
                ClientUserId = request.ClientUserId,
                AuthenticationMethod = "email",
                UserName = request.SignerName,
                Email = request.SignerEmail,
                RecipientId = request.RecipientId
            };
            var client = new RestClient (BaseUrl.Trim ('/'));
            var restrequest = new RestRequest ($"/envelopes/{request.EnvelopeId}/views/recipient", Method.POST);
            restrequest.JsonSerializer = new NewtonsoftJsonSerializer ();
            restrequest.RequestFormat = DataFormat.Json;
            restrequest.AddJsonBody (viewOptions);
            var response = ExecuteRequest<ViewUrl> (client, restrequest);
            return response;
        }

        private void ConfigureEvelope (ref EnvelopeDefinition envelope, IDocuSignTemplate docuSignTemplate) {

            envelope.BrandId = docuSignTemplate.BrandId;
            envelope.EmailSubject = docuSignTemplate.EmailSubject;

            if (docuSignTemplate.ExpirationSetting != null) {
                envelope.Notification = envelope.Notification ?? new Notification ();
                Expirations expiration = new Expirations ();
                expiration.ExpireAfter = docuSignTemplate.ExpirationSetting.ExpireAfterDays > 0 ? docuSignTemplate.ExpirationSetting.ExpireAfterDays.ToString () : null;
                expiration.ExpireWarn = docuSignTemplate.ExpirationSetting.WarningExpireBeforeDays > 0 ? docuSignTemplate.ExpirationSetting.WarningExpireBeforeDays.ToString () : null;
                expiration.ExpireEnabled = "true";
                envelope.Notification.Expirations = expiration;
            }
            if (docuSignTemplate.ReminderSetting != null) {
                envelope.Notification = envelope.Notification ?? new Notification ();
                Reminders reminder = new Reminders ();
                reminder.ReminderDelay = docuSignTemplate.ReminderSetting.FirstReminderAfterDays > 0 ? docuSignTemplate.ReminderSetting.FirstReminderAfterDays.ToString () : null;
                reminder.ReminderFrequency = docuSignTemplate.ReminderSetting.ReminderInterval > 0 ? docuSignTemplate.ReminderSetting.ReminderInterval.ToString () : null;
                reminder.ReminderEnabled = "true";
                envelope.Notification.Reminders = reminder;
            }
            if (docuSignTemplate.EventConfigurations != null) {
                EventNotification notification = new EventNotification ();
                notification.IncludeDocumentFields = docuSignTemplate.EventConfigurations.GetDocumentFields.ToString ();
                notification.IncludeDocuments = docuSignTemplate.EventConfigurations.GetDocuments.ToString ();
                notification.IncludeEnvelopeVoidReason = docuSignTemplate.EventConfigurations.GetRejectionReason.ToString ();
                notification.LoggingEnabled = docuSignTemplate.EventConfigurations.EnableLogging.ToString ();
                notification.Url = docuSignTemplate.EventConfigurations.ListnerUrl;
                notification.RecipientEvents = new List<Models.DocuSignModels.RecipientEvent> ();
                foreach (var recvrEvent in docuSignTemplate.EventConfigurations.RecipientEvent) {
                    Models.DocuSignModels.RecipientEvent rEvent = new Models.DocuSignModels.RecipientEvent () {
                        IncludeDocuments = recvrEvent.GetDocumetOnEvent.ToString (),
                        RecipientEventStatusCode = recvrEvent.EventType.ToString ()
                    };
                    notification.RecipientEvents.Add (rEvent);
                }
            }
        }
        private T ExecuteRequest<T> (IRestClient client, IRestRequest request) where T : class {
            string authHeader = "{\"Username\":\"" + Configuration.UserName +
                "\", \"Password\":\"" + Configuration.Password +
                "\", \"IntegratorKey\":\"" + Configuration.IntegratorKey + "\"}";
            request.AddHeader ("X-DocuSign-Authentication", authHeader);
            var response = client.Execute (request);
            if (response == null)
                throw new ArgumentNullException (nameof (response));
            try {
                return JsonConvert.DeserializeObject<T> (response.Content);
            } catch (Exception exception) {
                throw new Exception ("Unable to deserialize:" + response.Content, exception);
            }
        }
        #endregion

        public IGenerateEmbeddedViewResponse GenerateEmbeddedView (string entityId, IEmbedSigningRequest embedSigningRequest, string documentType) {
            #region Validation
            if (embedSigningRequest == null)
                throw new ArgumentException (nameof (embedSigningRequest));
            if (embedSigningRequest.TemplateDocuSignConfig == null)
                embedSigningRequest.TemplateDocuSignConfig = Configuration.DocumentSignature.Find (x => x.DocumentName == documentType);
            if (embedSigningRequest.File == null || embedSigningRequest.File.Count == 0)
                throw new ArgumentException (nameof (embedSigningRequest.File));

            #endregion
            DocuSignLogin ();
            var client = new RestClient (BaseUrl.Trim ('/'));
            var request = new RestRequest ("/envelopes", Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer ();
            var envelopeDefinations = CreateEnvelope (entityId, embedSigningRequest);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody (envelopeDefinations);
            var envelopeSummary = ExecuteRequest<EnvelopeSummary> (client, request);

            var response = new GenerateEmbeddedViewResponse {
                EnvelopeSummary = envelopeSummary
            };
            EnvelopeInfo envInfo = new EnvelopeInfo () {
                EntityId = entityId,
                EnvelopeId = envelopeSummary.EnvelopeId,
                Time = new TimeBucket (DateTime.Now)
            };
            envInfo.Signers = new List<ReceipientInfo> ();
            envInfo.Documents = new List<string> ();
            foreach (var rec in envelopeDefinations.Recipients.Signers) {
                ReceipientInfo recInfo = new ReceipientInfo () {
                    RoutingNumber = rec.RoutingOrder,
                    ReceipientId = rec.RecipientId,
                    Name = rec.Name
                };
                envInfo.Signers.Add (recInfo);
            }
            foreach (var doc in envelopeDefinations.Documents) {
                envInfo.Documents.Add (doc.Name);
            }
            DocusignRepository.AddEnvelopeInformation (envInfo);
            var recipientIds = envelopeDefinations?.Recipients?.Signers?.Select (x => x.RecipientId);
            if (recipientIds != null && recipientIds.Count () > 0) {
                response.RecepientIds = recipientIds.ToArray ();
            }
            return response;
        }

        public IGenerateEmbeddedUrlResponse GenerateEmbeddedUrl (IGenerateEmbeddedUrlProxyRequest request) {
            #region Validation
            if (request == null)
                throw new ArgumentException (nameof (request));

            if (string.IsNullOrWhiteSpace (request.ClientUserId))
                throw new ArgumentException (nameof (request));

            if (string.IsNullOrWhiteSpace (request.EnvelopeId))
                throw new ArgumentException (nameof (request.EnvelopeId));

            if (string.IsNullOrWhiteSpace (request.ReturnUrl))
                throw new ArgumentException (nameof (request.ReturnUrl));

            if (string.IsNullOrWhiteSpace (request.SignerEmail))
                throw new ArgumentException (nameof (request.SignerEmail));

            if (string.IsNullOrWhiteSpace (request.SignerName))
                throw new ArgumentException (nameof (request.SignerName));
            #endregion
            DocuSignLogin ();

            var result = GetViewUrl (request);
            return new GenerateEmbeddedUrlResponse () { Url = result?.Url };
        }

        public bool HasAuthenticated (string envelopeId) {

            EnvelopeAuditEventResponse response = GetAuthenticationInfo (envelopeId);
            bool hasAuthenticated = true;

            if (response != null && response.AuditEvents != null && response.AuditEvents.Count > 0) {
                foreach (var auditEvent in response.AuditEvents) {
                    var actions = auditEvent.EventFields.Where (d => d.Name.ToUpper () == "ACTION");

                    foreach (var eventField in actions) {
                        if (eventField.Name.ToUpper () == "ACTION" && eventField.Value.ToUpper () == "IDCHECK FAILED") {
                            hasAuthenticated = false;
                            break;
                        }
                    }

                    if (!hasAuthenticated) {
                        break;
                    }
                }
            }

            return hasAuthenticated;
        }

        public EnvelopeAuditEventResponse GetAuthenticationInfo (string envelopeId) {
            DocuSignLogin ();
            var client = new RestClient (BaseUrl.Trim ('/'));
            var request = new RestRequest ("envelopes/{envelopeId}/audit_events", Method.GET);
            request.AddUrlSegment ("envelopeId", envelopeId);
            var envelopeAuditEventResponse = ExecuteRequest<EnvelopeAuditEventResponse> (client, request);
            return envelopeAuditEventResponse;
        }

        public IGetEnvelopeInfoResponse GetEnvelopeInfo (string envelopeId) {
            DocuSignLogin ();
            var client = new RestClient (BaseUrl.Trim ('/'));
            var request = new RestRequest ("/envelopes/" + envelopeId, Method.GET);
            var envelope = ExecuteRequest<Envelope> (client, request);
            return new GetEnvelopeInfoResponse () { Envelope = envelope };
        }

        public List<DownloadDocument> DownloadDocuments (string envelopeId) {
            DocuSignLogin ();
            var client = new RestClient (BaseUrl.Trim ('/'));
            var request = new RestRequest ("/envelopes/{envelopeId}/documents", Method.GET);
            request.AddUrlSegment ("envelopeId", envelopeId);
            EnvelopeDocumentsResult docsList = ExecuteRequest<EnvelopeDocumentsResult> (client, request);
            // Download Envelope Document
            int docCount = docsList.EnvelopeDocuments.Count;
            List<DownloadDocument> responseData = new List<DownloadDocument> ();

            for (int i = 0; i < docCount; i++) {
                var docClient = new RestClient (BaseUrl.Trim ('/'));
                var docRrequest = new RestRequest ("/envelopes/{envelopeId}/documents/{documentId}", Method.GET);
                request.AddUrlSegment ("envelopeId", envelopeId);
                request.AddUrlSegment ("documentId", docsList.EnvelopeDocuments[i].DocumentId);
                var docStream = ExecuteRequest<object> (client, request);
                MemoryStream docMemStream = (MemoryStream) docStream;
                responseData.Add (new DownloadDocument {
                    Data = System.Convert.ToBase64String (docMemStream.ToArray ()),
                        FileName = docsList.EnvelopeDocuments[i].Name
                });
            }
            return responseData;
        }

        // Will return all the tabs with values filled for a specific envelope and by a specific receipient
        public dynamic GetReceipientData (string envelopeId, string receipientId) {
            DocuSignLogin ();
            var client = new RestClient (BaseUrl.Trim ('/'));
            var request = new RestRequest ("/envelopes/{envelopeId}/recipients/{recipientId}/tabs", Method.GET);
            request.AddUrlSegment ("envelopeId", envelopeId);
            request.AddUrlSegment ("recipientId", receipientId);
            var response = ExecuteRequest<dynamic> (client, request);
            return response;
        }

    }
}