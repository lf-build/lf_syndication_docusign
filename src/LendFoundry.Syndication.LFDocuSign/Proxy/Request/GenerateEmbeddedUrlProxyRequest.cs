﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Request
{
    public class GenerateEmbeddedUrlProxyRequest : IGenerateEmbeddedUrlProxyRequest
    {
        public string SignerEmail { get; set; }
        public string ClientUserId { get; set; }
        public string EnvelopeId { get; set; }
        public string ReturnUrl { get; set; }
        public string SignerName { get; set; }
        public string RecipientId { get; set; }
    }
}
