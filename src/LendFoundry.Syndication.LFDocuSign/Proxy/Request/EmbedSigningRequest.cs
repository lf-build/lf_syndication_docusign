﻿using LendFoundry.Syndication.LFDocuSign.Proxy.Models;
using LendFoundry.Syndication.LFDocuSign.TemplateConfiguration;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Request
{
    public class EmbedSigningRequest : IEmbedSigningRequest
    {
        public EmbedSigningRequest(LFDocuSign.Request.IEmbedSigningRequest request)
        {
            if (request != null)
            {
                ClientId = request.ClientId;
                EmailBody = request.EmailBody;
                if (request.Receipients != null && request.Receipients.Count > 0)
                    CopyReceipients(request.Receipients);
                if (request.CCReceipients != null && request.CCReceipients.Count > 0)
                    CopyCCReceipients(request.CCReceipients);
            }
        }
        public string ClientId { get; set; }
        public IList<IDocumentFile> File { get; set; }
        [JsonConverter(typeof(ConcreteListJsonConverter<List<Receipient>, IReceipient>))]
        public IList<IReceipient> Receipients { get; set; }
        [JsonConverter(typeof(ConcreteListJsonConverter<List<Receipient>, IReceipient>))]
        public IList<IReceipient> CCReceipients { get; set; }
        public IDocuSignTemplate TemplateDocuSignConfig { get; set; }
        public string EmailBody { get; set; }
        private void CopyReceipients(IList<LFDocuSign.Models.IReceipient> receipients)
        {
            Receipients = new List<IReceipient>();
            foreach (var receipient in receipients)
            {
                Receipients.Add(new Receipient(receipient));
            }
        }
        private void CopyCCReceipients(IList<LFDocuSign.Models.IReceipient> receipients)
        {
            CCReceipients = new List<IReceipient>();
            foreach (var receipient in receipients)
            {
                CCReceipients.Add(new Receipient(receipient));
            }
        }

    }
}
