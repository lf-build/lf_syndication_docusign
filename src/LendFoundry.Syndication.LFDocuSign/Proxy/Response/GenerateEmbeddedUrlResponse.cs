﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Response
{
    public class GenerateEmbeddedUrlResponse: IGenerateEmbeddedUrlResponse
    {
        public string Url { get; set; }
    }
}
