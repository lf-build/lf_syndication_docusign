﻿using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Response
{
    public class GetEnvelopeInfoResponse: IGetEnvelopeInfoResponse
    {
        public Envelope Envelope { get; set; }
    }
}
