﻿using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Response
{
    public class GenerateEmbeddedViewResponse: IGenerateEmbeddedViewResponse
    {
        public IEnvelopeSummary EnvelopeSummary { get; set; }
        public string[] RecepientIds { get; set; }
    }
}
