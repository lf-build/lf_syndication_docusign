﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models
{
    public class DocumentFile : IDocumentFile
    {
        public byte[] FileBtyes { get; set; }
        public string DocumentId { get; set; }
        public string DocumentName { get; set; }
    }
}
