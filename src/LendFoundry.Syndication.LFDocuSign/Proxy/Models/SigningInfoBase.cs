﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models
{
    public class SigningInfoBase : ISigningInfoBase
    {
        public SigningInfoBase(LFDocuSign.Models.ISigningInfoBase signingInfoBase)
        {
            if (signingInfoBase != null)
            {
                PageNumber = signingInfoBase.PageNumber;
                XPosition = signingInfoBase.XPosition;
                YPosition = signingInfoBase.YPosition;
            }
        }
        public string PageNumber { get; set; }
        public string XPosition { get; set; }
        public string YPosition { get; set; }
    }
}
