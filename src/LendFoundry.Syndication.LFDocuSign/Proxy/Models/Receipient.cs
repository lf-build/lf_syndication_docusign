﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models {
    public class Receipient : IReceipient {
        public Receipient (LFDocuSign.Models.IReceipient receipient) {
            if (receipient != null) {
                Name = receipient.Name;
                Email = receipient.Email;
                ClientId = receipient.ClientId;
                RoutingNumber = receipient.RoutingNumber;
                RecipientId = receipient.RecipientId;
            }
        }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ClientId { get; set; }
        public int RoutingNumber { get; set; }
        public string RecipientId { get; set; }
    }
}