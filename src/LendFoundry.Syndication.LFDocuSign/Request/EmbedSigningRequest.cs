﻿using LendFoundry.Syndication.LFDocuSign.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.Request
{
    public class EmbedSigningRequest : IEmbedSigningRequest
    {
        public List<string> DocumentId { get; set; }
        public string ClientId { get; set; }
        [JsonConverter(typeof(ConcreteListJsonConverter<List<Receipient>, IReceipient>))]
        public IList<IReceipient> Receipients { get; set; }
        [JsonConverter(typeof(ConcreteListJsonConverter<List<Receipient>, IReceipient>))]
        public IList<IReceipient> CCReceipients { get; set; }
        public string EmailBody { get; set; }
    }
}
