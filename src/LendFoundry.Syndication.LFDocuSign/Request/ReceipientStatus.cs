using LendFoundry.Foundation.Date;

namespace LendFoundry.Syndication.LFDocuSign.Request {
 
    public class ReceipientStatus : IReceipientStatus {
        public string Name { get; set; }
        public string Email { get; set; }
        public string RecipientId { get; set; }
        public string Status { get; set; }
        
      public  string SignedOn { get; set; }
    }
}