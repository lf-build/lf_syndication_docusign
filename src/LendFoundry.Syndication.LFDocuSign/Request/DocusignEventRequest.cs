﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Request {
    public class DocusignEventRequest : IDocusignEventRequest {
        public List<string> SignerEmail { get; set; }
        public List<string> DocumentBytes { get; set; }
        public string Status { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public List<string> DocumentName { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IReceipientStatus, ReceipientStatus>))]
        public List<IReceipientStatus> ReceipientStatus { get; set; }
    }

   
}