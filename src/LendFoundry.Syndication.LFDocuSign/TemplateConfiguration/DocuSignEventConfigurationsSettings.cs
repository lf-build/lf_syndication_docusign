﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public class DocuSignEventConfigurationsSettings : IDocuSignEventConfigurationsSettings
    {
        public string ListnerUrl { get; set; }
        public bool GetDocumentFields { get; set; }
        public bool GetDocuments { get; set; }
        public bool GetRejectionReason { get; set; }
        public bool EnableLogging { get; set; } = true;
        [JsonConverter(typeof(ConcreteListJsonConverter<List<RecipientEvent>, IRecipientEvent>))]
        public IList<IRecipientEvent> RecipientEvent { get; set; }
    }
}
