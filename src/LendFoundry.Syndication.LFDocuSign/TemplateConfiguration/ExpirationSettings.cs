﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public class ExpirationSettings: IExpirationSettings
    {
        public int ExpireAfterDays { get; set; }
        public int WarningExpireBeforeDays { get; set; }
    }
}
