﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public class ReminderSettings : IReminderSettings
    {
        public int FirstReminderAfterDays { get; set; }
        public int ReminderInterval { get; set; }
    }
}
