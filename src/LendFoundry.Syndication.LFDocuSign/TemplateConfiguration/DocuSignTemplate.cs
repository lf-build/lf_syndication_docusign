﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public class DocuSignTemplate : IDocuSignTemplate
    {
        public string ReturnUrl { get; set; }
        public string BrandId { get; set; }
        public string DocumentName { get; set; }
        public string EmailSubject { get; set; }
        [JsonConverter(typeof(ConcreteJsonConverter<DocuSignEventConfigurationsSettings>))]
        public IDocuSignEventConfigurationsSettings EventConfigurations { get; set; }
        [JsonConverter(typeof(ConcreteJsonConverter<ExpirationSettings>))]
        public IExpirationSettings ExpirationSetting { get; set; }
        [JsonConverter(typeof(ConcreteJsonConverter<ReminderSettings>))]
        public IReminderSettings ReminderSetting { get; set; }

        [JsonConverter(typeof(ConcreteListJsonConverter<List<SignerConfiguration>, ISignerConfiguration>))]
        public IList<ISignerConfiguration> SignerConfiguration { get; set; }
    }
}
