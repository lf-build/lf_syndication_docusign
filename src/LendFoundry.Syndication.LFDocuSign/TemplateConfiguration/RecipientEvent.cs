﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public class RecipientEvent: IRecipientEvent
    {
        public DocusignEventsType EventType { get; set; }
        public bool GetDocumetOnEvent { get; set; }
    }
}
