﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public class SignerConfiguration: ISignerConfiguration
    {
        public int? RoutingOrder { get; set; }
        public string RecipientId { get; set; }
        [JsonConverter(typeof(ConcreteListJsonConverter<List<DocuSignTab>, IDocuSignTab>))]
        public IList<IDocuSignTab> Tabs { get; set; }
    }
}
