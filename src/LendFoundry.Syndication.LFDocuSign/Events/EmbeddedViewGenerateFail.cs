﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.LFDocuSign.Events
{
    public class EmbeddedViewGenerateFail : SyndicationCalledEvent
    {
    }
}
