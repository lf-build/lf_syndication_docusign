﻿using System;
using System.Linq;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.LFDocuSign.Request;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.Syndication.LFDocuSign {
    /// <summary>
    /// Class DocuSignService.
    /// </summary>
    /// <seealso cref="LendFoundry.Syndication.DocuSign.IDocuSignService" />
    public class DocuSignListener : IDocuSignListener {
        #region Constructor
        public DocuSignListener
            (
                ITokenHandler tokenHandler,
                IEventHubClientFactory eventHubFactory,
                ILoggerFactory loggerFactory,
                ITenantServiceFactory tenantServiceFactory,
                IConfigurationServiceFactory configurationFactory,
                ITenantTimeFactory tenantTimeFactory,
                IDocusignServiceFactory docusignServiceFactory,
                IDocusignReceipientRepositoryFactory docusignReceipientRepositoryFactory) {

                EnsureParameter (nameof (tokenHandler), tokenHandler);
                EnsureParameter (nameof (loggerFactory), loggerFactory);
                EnsureParameter (nameof (eventHubFactory), eventHubFactory);
                EnsureParameter (nameof (tenantServiceFactory), tenantServiceFactory);
                EnsureParameter (nameof (configurationFactory), configurationFactory);
                EnsureParameter (nameof (tenantTimeFactory), tenantTimeFactory);
                EnsureParameter (nameof (docusignServiceFactory), docusignServiceFactory);
                EnsureParameter (nameof (docusignReceipientRepositoryFactory), docusignReceipientRepositoryFactory);

                TokenHandler = tokenHandler;
                LoggerFactory = loggerFactory;
                EventHubFactory = eventHubFactory;
                TenantServiceFactory = tenantServiceFactory;
                ConfigurationFactory = configurationFactory;
                TenantTimeFactory = tenantTimeFactory;
                DocusignServiceFactory = docusignServiceFactory;
                DocusignReceipientRepositoryFactory = docusignReceipientRepositoryFactory;
            }
        #endregion

        #region Private Properties
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IDocusignServiceFactory DocusignServiceFactory { get; }
        private IDocusignReceipientRepositoryFactory DocusignReceipientRepositoryFactory { get; }
        #endregion

        #region Public Methods

        public void Start () {
            var logger = LoggerFactory.Create (NullLogContext.Instance);
            logger.Info ("Starting Listener...");

            try {
                var emptyReader = new StaticTokenReader (string.Empty);
                var tenantService = TenantServiceFactory.Create (emptyReader);
                var tenants = tenantService.GetActiveTenants ();
                logger.Info ($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach (tenant => {
                    logger.Info ($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue (tenant.Id, Settings.ServiceName, null, "system", null);
                    var reader = new StaticTokenReader (token.Value);
                    var eventHub = EventHubFactory.Create (reader);
                    var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                    var configuration = ConfigurationFactory.Create<DocuSignConfiguration> (Settings.ServiceName, reader).Get ();
                    var docusignService = DocusignServiceFactory.Create (reader, logger);
                    if (configuration != null && configuration.events != null) {
                        configuration
                            .events
                            .ToList ().ForEach (events => {
                                eventHub.On (events.Name, ProcessEvent (logger, docusignService));
                                logger.Info ($"It was made subscription to EventHub with the Event: #{events.Name}");
                            });

                        logger.Info ("-------------------------------------------");
                    } else {
                        logger.Error ($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                    }
                    eventHub.StartAsync ();
                });

                logger.Info ("DocuSign listener started");
            } catch (Exception ex) {
                logger.Error ("Error while listening eventhub to process DocuSign listener", ex);
                logger.Info ("\n DocuSign listener  is working yet and waiting new event\n");
            }
        }

        #endregion

        #region Private Methods

        #region Process Event
        private Action<EventInfo> ProcessEvent (
            ILogger logger,
            IDocuSignService docusignService) {
            return async @event =>
            {
                try
                {
                    logger.Info (@event.Name, @event.Data);
                    await docusignService.ProcessEvent (@event);
                }
                catch (Exception ex){
                logger.Error ($"Unhadled exception while listening event {@event.Name}",ex);
                  }

    };
}
#endregion

#region Ensure Parameter
private static void EnsureParameter (string name, object value) {
    if (value == null) throw new ArgumentNullException ($"{name} cannot be null.");
}
#endregion

#endregion
}
}