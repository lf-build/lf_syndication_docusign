﻿using LendFoundry.Configuration;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Logging;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Date;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Syndication.LFDocuSign
{
    public class DocusignServiceFactory: IDocusignServiceFactory
    {
        public DocusignServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }
        private IServiceProvider Provider { get; }

        public IDocuSignService Create(ITokenReader reader, ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var docusignConfigurationService = configurationServiceFactory.Create<DocuSignConfiguration>(Settings.ServiceName, reader);
            var docusignConfiguration = docusignConfigurationService.Get();

            var decisionEngineFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngine = decisionEngineFactory.Create(reader);

            var eventhubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventhub=eventhubFactory.Create(reader);

            var repositoryFactory = Provider.GetService<IDocusignReceipientRepositoryFactory>();
            var docusignReceipientRepository = repositoryFactory.Create(reader);

            return new DocuSignService(eventhub, logger, decisionEngine, docusignConfiguration, docusignReceipientRepository);
        }
    }
}
