﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.LFDocuSign
{
    public static class DocuSignListenerExtensions
    {
        public static void UseDocuSignListener(this IApplicationBuilder application)
        {
            application.ApplicationServices.GetRequiredService<IDocuSignListener>().Start();
        }
    }
}