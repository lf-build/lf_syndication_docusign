namespace LendFoundry.Syndication.LFDocuSign.Models
{
    public class EnvelopeSummary : ErrorDetails, IEnvelopeSummary
    {
        public EnvelopeSummary(Proxy.Models.DocuSignModels.IEnvelopeSummary envSummary) : base(envSummary)
        {
            EnvelopeId = envSummary?.EnvelopeId;
            Uri = envSummary?.Uri;
            StatusDateTime = envSummary?.StatusDateTime;
            Status = envSummary?.Status;
        }
        public string EnvelopeId { get; set; }

        public string Uri { get; set; }

        public string StatusDateTime { get; set; }

        public string Status { get; set; }
    }

}

