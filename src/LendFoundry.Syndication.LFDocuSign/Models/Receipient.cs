﻿namespace LendFoundry.Syndication.LFDocuSign.Models {
    public class Receipient : IReceipient {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ClientId { get; set; }
        public int RoutingNumber { get; set; }
        public string RecipientId { get; set; }
    }
}