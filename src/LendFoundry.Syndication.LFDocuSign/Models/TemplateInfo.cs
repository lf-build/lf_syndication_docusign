﻿namespace LendFoundry.Syndication.LFDocuSign.Models
{
    public class TemplateInfo: ITemplateInfo
    {
        public string TemplateName { get; set; }
        public string TemplateVersion { get; set; }
    }
}
