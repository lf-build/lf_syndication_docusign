﻿namespace LendFoundry.Syndication.LFDocuSign.Models
{
    public class Envelope : IEnvelope
    {
        public Envelope(Proxy.Models.DocuSignModels.IEnvelope envelop)
        {
            Status = envelop?.Status;
            DocumentsUri = envelop?.DocumentsUri;
            TransactionId = envelop?.TransactionId;
            RecipientsUri = envelop?.RecipientsUri;
            Asynchronous = envelop?.Asynchronous;
            EnvelopeUri = envelop?.EnvelopeUri;
            EmailSubject = envelop?.EmailSubject;
            EmailBlurb = envelop?.EmailBlurb;
            EnvelopeId = envelop?.EnvelopeId;
            SigningLocation = envelop?.SigningLocation;
            CustomFieldsUri = envelop?.CustomFieldsUri;
            EnvelopeIdStamping = envelop?.EnvelopeIdStamping;
            AuthoritativeCopy = envelop?.AuthoritativeCopy;
            NotificationUri = envelop?.NotificationUri;
            EnforceSignerVisibility = envelop?.EnforceSignerVisibility;
            EnableWetSign = envelop?.EnableWetSign;
            AllowMarkup = envelop?.AllowMarkup;
            AllowReassign = envelop?.AllowReassign;
            CreatedDateTime = envelop?.CreatedDateTime;
            LastModifiedDateTime = envelop?.LastModifiedDateTime;
            DeliveredDateTime = envelop?.DeliveredDateTime;
            SentDateTime = envelop?.SentDateTime;
            CompletedDateTime = envelop?.CompletedDateTime;
            VoidedDateTime = envelop?.VoidedDateTime;
            VoidedReason = envelop?.VoidedReason;
            DeletedDateTime = envelop?.DeletedDateTime;
            DeclinedDateTime = envelop?.DeclinedDateTime;
            StatusChangedDateTime = envelop?.StatusChangedDateTime;
            DocumentsCombinedUri = envelop?.DocumentsCombinedUri;
            CertificateUri = envelop?.CertificateUri;
            TemplatesUri = envelop?.TemplatesUri;
            MessageLock = envelop?.MessageLock;
            RecipientsLock = envelop?.RecipientsLock;
            UseDisclosure = envelop?.UseDisclosure;
            Notification = envelop?.Notification;
            EmailSettings = envelop?.EmailSettings;
            PurgeState = envelop?.PurgeState;
            LockInformation = envelop?.LockInformation;
            Is21CFRPart11 = envelop?.Is21CFRPart11;
        }
        public string Status { get; set; }
        public string DocumentsUri { get; set; }
        public string TransactionId { get; set; }
        public string RecipientsUri { get; set; }
        public string Asynchronous { get; set; }
        public string EnvelopeUri { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBlurb { get; set; }
        public string EnvelopeId { get; set; }
        public string SigningLocation { get; set; }
        public string CustomFieldsUri { get; set; }
        public string EnvelopeIdStamping { get; set; }
        public string AuthoritativeCopy { get; set; }
        public string NotificationUri { get; set; }
        public string EnforceSignerVisibility { get; set; }
        public string EnableWetSign { get; set; }
        public string AllowMarkup { get; set; }
        public string AllowReassign { get; set; }
        public string CreatedDateTime { get; set; }
        public string LastModifiedDateTime { get; set; }
        public string DeliveredDateTime { get; set; }
        public string SentDateTime { get; set; }
        public string CompletedDateTime { get; set; }
        public string VoidedDateTime { get; set; }
        public string VoidedReason { get; set; }
        public string DeletedDateTime { get; set; }
        public string DeclinedDateTime { get; set; }
        public string StatusChangedDateTime { get; set; }
        public string DocumentsCombinedUri { get; set; }
        public string CertificateUri { get; set; }
        public string TemplatesUri { get; set; }
        public string MessageLock { get; set; }
        public string RecipientsLock { get; set; }
        public string UseDisclosure { get; set; }
        public object Notification { get; set; }
        public object EmailSettings { get; set; }
        public string PurgeState { get; set; }
        public object LockInformation { get; set; }
        public string Is21CFRPart11 { get; set; }
    }
}
