namespace LendFoundry.Syndication.LFDocuSign.Models
{
    public class ErrorDetails :IErrorDetails
    {
        public ErrorDetails(Proxy.Models.DocuSignModels.IErrorDetails errorDetails)
        {
            ErrorCode = errorDetails?.ErrorCode;
            Message = errorDetails?.Message;
        }
        public string ErrorCode { get; set; }
        
        public string Message { get; set; }
    }
}
