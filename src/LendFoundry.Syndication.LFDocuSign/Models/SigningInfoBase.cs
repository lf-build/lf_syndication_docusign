﻿namespace LendFoundry.Syndication.LFDocuSign.Models
{
    public class SigningInfoBase: ISigningInfoBase
    {
        public string PageNumber { get; set; }
        public string XPosition { get; set; }
        public string YPosition { get; set; }
    }
}
