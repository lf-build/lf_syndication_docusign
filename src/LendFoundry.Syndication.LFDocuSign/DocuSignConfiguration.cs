﻿using LendFoundry.Syndication.LFDocuSign.TemplateConfiguration;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.LFDocuSign
{
    public class DocuSignConfiguration : IDocuSignConfiguration, IDependencyConfiguration
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string IntegratorKey { get; set; }
        public string Url { get; set; }
        [JsonConverter(typeof(ConcreteListJsonConverter<List<DocuSignTemplate>, IDocuSignTemplate>))]
        public List<IDocuSignTemplate> DocumentSignature {get;set;}
        public List<EventConfiguration> events { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
