﻿using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;
using LendFoundry.Syndication.LFDocuSign.Request;
using LendFoundry.Syndication.LFDocuSign.Response;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Syndication.LFDocuSign.Models;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.Syndication.LFDocuSign
{
    public interface IDocuSignService
    {
        Task<IGenerateEmbeddedViewResponse> GenerateEmbeddedView(string entityType, string entityId, string documentType, IEmbedSigningRequest request);

        Task<IEmbeddedUrlResponse> GenerateEmbeddedUrl(string entityType, string entityId, IGenerateEmbeddedUrlRequest request);

        Task<bool> HasAuthenticated(string entityType, string entityId, string envelopeId);

        Task<EnvelopeAuditEventResponse> GetAuthenticationInfo(string entityType, string entityId, string envelopeId);

        Task<IGetEnvelopeInfoResponse> GetEnvelopeInfo(string entityType, string entityId, string envelopeId);

        Task<List<DownloadDocument>> DownloadDocuments(string entityType, string entityId, string envelopeId);
        Task ProcessEvent(EventInfo eventInfo);
        Task<dynamic> GetReceipientData(string receipientId, string entityId, string envelopeId);

        Task<List<IEnvelopeInfo>> GetEnvelopesByApplicationNumber(string entityId);
        Task<List<IReceipientDetails>> GetReceipientByEntityId (string entityId);
       Task<IReceipientDetails> UpdateReceipientDetails (string entityType, string entityId,IReceipientDetails receipientDetailRequest);
    }
}