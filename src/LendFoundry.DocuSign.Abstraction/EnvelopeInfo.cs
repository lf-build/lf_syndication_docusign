﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;
using LendFoundry.Syndication.LFDocuSign.TemplateConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LFDocuSign
{
    public class EnvelopeInfo : Aggregate, IEnvelopeInfo
    {
        public string EntityId { get; set; }
        public List<string> Documents{ get; set; }
        public string EnvelopeId { get; set; }
        public List<ReceipientInfo> Signers { get; set; }
        public TimeBucket Time { get; set; }
    }
}
