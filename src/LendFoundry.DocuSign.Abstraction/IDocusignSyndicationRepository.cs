﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LFDocuSign
{
    public interface IDocusignSyndicationRepository : IRepository<IEnvelopeInfo>
    {
        Task<List<IEnvelopeInfo>> GetEnvelopesInformation(string EntityId);
        Task AddEnvelopeInformation(IEnvelopeInfo envelopeInfo);
        
    }
}
