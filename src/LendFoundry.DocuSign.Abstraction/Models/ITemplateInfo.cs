﻿namespace LendFoundry.Syndication.LFDocuSign.Models
{
    public interface ITemplateInfo
    {
        string TemplateName { get; set; }
        string TemplateVersion { get; set; }
    }
}
