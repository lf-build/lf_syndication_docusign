﻿namespace LendFoundry.Syndication.LFDocuSign.Models
{
    public interface IErrorDetails
    {
        string ErrorCode { get; set; }
        string Message { get; set; }
    }
}
