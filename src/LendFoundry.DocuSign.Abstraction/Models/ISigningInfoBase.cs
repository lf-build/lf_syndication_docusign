﻿namespace LendFoundry.Syndication.LFDocuSign.Models
{
    public interface ISigningInfoBase
    {
        string PageNumber { get; set; }
        string XPosition { get; set; }
        string YPosition { get; set; }
    }
}
