﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Syndication.LFDocuSign.Models {
    public class ReceipientDetails : Aggregate, IReceipientDetails {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string RecipientId { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public Status Status { get; set; }
        public int SendCount { get; set; }
        public TimeBucket Time { get; set; }

      public  TimeBucket SignedOn { get; set; }
        
    }
}