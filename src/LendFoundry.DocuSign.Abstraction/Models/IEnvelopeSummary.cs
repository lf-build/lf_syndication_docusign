﻿namespace LendFoundry.Syndication.LFDocuSign.Models
{
    public interface IEnvelopeSummary : IErrorDetails
    {
        string EnvelopeId { get; set; }
        string Uri { get; set; }
        string StatusDateTime { get; set; }
        string Status { get; set; }
    }
}
