﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Syndication.LFDocuSign.Models {
    public interface IReceipientDetails : IAggregate {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string RecipientId { get; set; }
        string Name { get; set; }
        string Email { get; set; }
        Status Status { get; set; }
        int SendCount { get; set; }
        TimeBucket Time { get; set; }
        TimeBucket SignedOn { get; set; }
    }
}