using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;


namespace  LendFoundry.Syndication.LFDocuSign
{
    public interface IDocusignReceipientRepositoryFactory
    {
#region Public Methods
        IDocusignReceipientRepository Create(ITokenReader reader);
#endregion
    }
}