﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign {
    public class EventConfiguration {
        public string Response { get; set; }
        public string Name { get; set; }
        public List<string> CompletionEvents { get; set; }
        public string Rule { get; set; }
        public string ReceipientRule { get; set; }
    }
}