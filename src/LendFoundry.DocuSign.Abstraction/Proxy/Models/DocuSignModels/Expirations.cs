using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public class Expirations 
    {

        [JsonProperty(PropertyName="expireEnabled")]
        public string ExpireEnabled { get; set; }

        [JsonProperty(PropertyName="expireAfter")]
        public string ExpireAfter { get; set; }

        [JsonProperty(PropertyName="expireWarn")]
        public string ExpireWarn { get; set; }
    }
}
