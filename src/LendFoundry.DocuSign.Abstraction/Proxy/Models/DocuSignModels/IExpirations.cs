﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public interface IExpirations
    {
        string ExpireEnabled { get; set; }
        string ExpireAfter { get; set; }
        string ExpireWarn { get; set; }
    }
}
