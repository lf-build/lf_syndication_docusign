using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{ 
    public class LockInformation : ILockInformation
    {

        [JsonProperty(PropertyName="lockedByUser")]
        public UserInfo LockedByUser { get; set; }

        [JsonProperty(PropertyName="lockedByApp")]
        public string LockedByApp { get; set; }

        [JsonProperty(PropertyName="lockedUntilDateTime")]
        public string LockedUntilDateTime { get; set; }

        [JsonProperty(PropertyName="lockDurationInSeconds")]
        public string LockDurationInSeconds { get; set; }

        [JsonProperty(PropertyName="lockType")]
        public string LockType { get; set; }

        [JsonProperty(PropertyName="useScratchPad")]
        public string UseScratchPad { get; set; }

        [JsonProperty(PropertyName="lockToken")]
        public string LockToken { get; set; }

        [JsonProperty(PropertyName="errorDetails")]
        public ErrorDetails ErrorDetails { get; set; }

    }
}
