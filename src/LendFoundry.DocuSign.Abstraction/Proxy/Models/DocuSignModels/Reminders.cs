using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public class Reminders 
    {
        [JsonProperty(PropertyName="reminderEnabled")]
        public string ReminderEnabled { get; set; }

        [JsonProperty(PropertyName="reminderDelay")]
        public string ReminderDelay { get; set; }

        [JsonProperty(PropertyName="reminderFrequency")]
        public string ReminderFrequency { get; set; }
    }
}
