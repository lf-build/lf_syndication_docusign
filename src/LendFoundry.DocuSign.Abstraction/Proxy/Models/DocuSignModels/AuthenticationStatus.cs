using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{

    /// <summary>
    /// 
    /// </summary>
    
    public class AuthenticationStatus 
    {
        
        /// <summary>
        /// Gets or Sets AccessCodeResult
        /// </summary>
        [JsonProperty(PropertyName="accessCodeResult")]
        public EventResult AccessCodeResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets PhoneAuthResult
        /// </summary>
        [JsonProperty(PropertyName="phoneAuthResult")]
        public EventResult PhoneAuthResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets IdLookupResult
        /// </summary>
        [JsonProperty(PropertyName="idLookupResult")]
        public EventResult IdLookupResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets IdQuestionsResult
        /// </summary>
        [JsonProperty(PropertyName="idQuestionsResult")]
        public EventResult IdQuestionsResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets AgeVerifyResult
        /// </summary>
        [JsonProperty(PropertyName="ageVerifyResult")]
        public EventResult AgeVerifyResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets STANPinResult
        /// </summary>
        [JsonProperty(PropertyName="sTANPinResult")]
        public EventResult STANPinResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets OfacResult
        /// </summary>
        [JsonProperty(PropertyName="ofacResult")]
        public EventResult OfacResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets LiveIDResult
        /// </summary>
        [JsonProperty(PropertyName="liveIDResult")]
        public EventResult LiveIDResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets FacebookResult
        /// </summary>
        [JsonProperty(PropertyName="facebookResult")]
        public EventResult FacebookResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets GoogleResult
        /// </summary>
        [JsonProperty(PropertyName="googleResult")]
        public EventResult GoogleResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets LinkedinResult
        /// </summary>
        [JsonProperty(PropertyName="linkedinResult")]
        public EventResult LinkedinResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets SalesforceResult
        /// </summary>
        [JsonProperty(PropertyName="salesforceResult")]
        public EventResult SalesforceResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets TwitterResult
        /// </summary>
        [JsonProperty(PropertyName="twitterResult")]
        public EventResult TwitterResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets OpenIDResult
        /// </summary>
        [JsonProperty(PropertyName="openIDResult")]
        public EventResult OpenIDResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets AnySocialIDResult
        /// </summary>
        [JsonProperty(PropertyName="anySocialIDResult")]
        public EventResult AnySocialIDResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets YahooResult
        /// </summary>
        [JsonProperty(PropertyName="yahooResult")]
        public EventResult YahooResult { get; set; }
  
        
        /// <summary>
        /// Gets or Sets SmsAuthResult
        /// </summary>
        [JsonProperty(PropertyName="smsAuthResult")]
        public EventResult SmsAuthResult { get; set; }
  
        
  
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class AuthenticationStatus {\n");
            sb.Append("  AccessCodeResult: ").Append(AccessCodeResult).Append("\n");
            sb.Append("  PhoneAuthResult: ").Append(PhoneAuthResult).Append("\n");
            sb.Append("  IdLookupResult: ").Append(IdLookupResult).Append("\n");
            sb.Append("  IdQuestionsResult: ").Append(IdQuestionsResult).Append("\n");
            sb.Append("  AgeVerifyResult: ").Append(AgeVerifyResult).Append("\n");
            sb.Append("  STANPinResult: ").Append(STANPinResult).Append("\n");
            sb.Append("  OfacResult: ").Append(OfacResult).Append("\n");
            sb.Append("  LiveIDResult: ").Append(LiveIDResult).Append("\n");
            sb.Append("  FacebookResult: ").Append(FacebookResult).Append("\n");
            sb.Append("  GoogleResult: ").Append(GoogleResult).Append("\n");
            sb.Append("  LinkedinResult: ").Append(LinkedinResult).Append("\n");
            sb.Append("  SalesforceResult: ").Append(SalesforceResult).Append("\n");
            sb.Append("  TwitterResult: ").Append(TwitterResult).Append("\n");
            sb.Append("  OpenIDResult: ").Append(OpenIDResult).Append("\n");
            sb.Append("  AnySocialIDResult: ").Append(AnySocialIDResult).Append("\n");
            sb.Append("  YahooResult: ").Append(YahooResult).Append("\n");
            sb.Append("  SmsAuthResult: ").Append(SmsAuthResult).Append("\n");
            
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as AuthenticationStatus);
        }

        /// <summary>
        /// Returns true if AuthenticationStatus instances are equal
        /// </summary>
        /// <param name="other">Instance of AuthenticationStatus to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(AuthenticationStatus other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return 
                (
                    this.AccessCodeResult == other.AccessCodeResult ||
                    this.AccessCodeResult != null &&
                    this.AccessCodeResult.Equals(other.AccessCodeResult)
                ) && 
                (
                    this.PhoneAuthResult == other.PhoneAuthResult ||
                    this.PhoneAuthResult != null &&
                    this.PhoneAuthResult.Equals(other.PhoneAuthResult)
                ) && 
                (
                    this.IdLookupResult == other.IdLookupResult ||
                    this.IdLookupResult != null &&
                    this.IdLookupResult.Equals(other.IdLookupResult)
                ) && 
                (
                    this.IdQuestionsResult == other.IdQuestionsResult ||
                    this.IdQuestionsResult != null &&
                    this.IdQuestionsResult.Equals(other.IdQuestionsResult)
                ) && 
                (
                    this.AgeVerifyResult == other.AgeVerifyResult ||
                    this.AgeVerifyResult != null &&
                    this.AgeVerifyResult.Equals(other.AgeVerifyResult)
                ) && 
                (
                    this.STANPinResult == other.STANPinResult ||
                    this.STANPinResult != null &&
                    this.STANPinResult.Equals(other.STANPinResult)
                ) && 
                (
                    this.OfacResult == other.OfacResult ||
                    this.OfacResult != null &&
                    this.OfacResult.Equals(other.OfacResult)
                ) && 
                (
                    this.LiveIDResult == other.LiveIDResult ||
                    this.LiveIDResult != null &&
                    this.LiveIDResult.Equals(other.LiveIDResult)
                ) && 
                (
                    this.FacebookResult == other.FacebookResult ||
                    this.FacebookResult != null &&
                    this.FacebookResult.Equals(other.FacebookResult)
                ) && 
                (
                    this.GoogleResult == other.GoogleResult ||
                    this.GoogleResult != null &&
                    this.GoogleResult.Equals(other.GoogleResult)
                ) && 
                (
                    this.LinkedinResult == other.LinkedinResult ||
                    this.LinkedinResult != null &&
                    this.LinkedinResult.Equals(other.LinkedinResult)
                ) && 
                (
                    this.SalesforceResult == other.SalesforceResult ||
                    this.SalesforceResult != null &&
                    this.SalesforceResult.Equals(other.SalesforceResult)
                ) && 
                (
                    this.TwitterResult == other.TwitterResult ||
                    this.TwitterResult != null &&
                    this.TwitterResult.Equals(other.TwitterResult)
                ) && 
                (
                    this.OpenIDResult == other.OpenIDResult ||
                    this.OpenIDResult != null &&
                    this.OpenIDResult.Equals(other.OpenIDResult)
                ) && 
                (
                    this.AnySocialIDResult == other.AnySocialIDResult ||
                    this.AnySocialIDResult != null &&
                    this.AnySocialIDResult.Equals(other.AnySocialIDResult)
                ) && 
                (
                    this.YahooResult == other.YahooResult ||
                    this.YahooResult != null &&
                    this.YahooResult.Equals(other.YahooResult)
                ) && 
                (
                    this.SmsAuthResult == other.SmsAuthResult ||
                    this.SmsAuthResult != null &&
                    this.SmsAuthResult.Equals(other.SmsAuthResult)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                
                if (this.AccessCodeResult != null)
                    hash = hash * 57 + this.AccessCodeResult.GetHashCode();
                
                if (this.PhoneAuthResult != null)
                    hash = hash * 57 + this.PhoneAuthResult.GetHashCode();
                
                if (this.IdLookupResult != null)
                    hash = hash * 57 + this.IdLookupResult.GetHashCode();
                
                if (this.IdQuestionsResult != null)
                    hash = hash * 57 + this.IdQuestionsResult.GetHashCode();
                
                if (this.AgeVerifyResult != null)
                    hash = hash * 57 + this.AgeVerifyResult.GetHashCode();
                
                if (this.STANPinResult != null)
                    hash = hash * 57 + this.STANPinResult.GetHashCode();
                
                if (this.OfacResult != null)
                    hash = hash * 57 + this.OfacResult.GetHashCode();
                
                if (this.LiveIDResult != null)
                    hash = hash * 57 + this.LiveIDResult.GetHashCode();
                
                if (this.FacebookResult != null)
                    hash = hash * 57 + this.FacebookResult.GetHashCode();
                
                if (this.GoogleResult != null)
                    hash = hash * 57 + this.GoogleResult.GetHashCode();
                
                if (this.LinkedinResult != null)
                    hash = hash * 57 + this.LinkedinResult.GetHashCode();
                
                if (this.SalesforceResult != null)
                    hash = hash * 57 + this.SalesforceResult.GetHashCode();
                
                if (this.TwitterResult != null)
                    hash = hash * 57 + this.TwitterResult.GetHashCode();
                
                if (this.OpenIDResult != null)
                    hash = hash * 57 + this.OpenIDResult.GetHashCode();
                
                if (this.AnySocialIDResult != null)
                    hash = hash * 57 + this.AnySocialIDResult.GetHashCode();
                
                if (this.YahooResult != null)
                    hash = hash * 57 + this.YahooResult.GetHashCode();
                
                if (this.SmsAuthResult != null)
                    hash = hash * 57 + this.SmsAuthResult.GetHashCode();
                
                return hash;
            }
        }

    }
}
