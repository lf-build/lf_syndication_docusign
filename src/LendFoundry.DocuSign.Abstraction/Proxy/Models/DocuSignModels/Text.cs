using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{

    /// <summary>
    /// 
    /// </summary>
    
    public class Text 
    {

        [JsonProperty(PropertyName = "height")]
        public int? Height { get; set; } = 15;
        [JsonProperty(PropertyName = "width")]
        public int? Width { get; set; }=100;
        //[JsonProperty(PropertyName = "maxLength")]
        //public int? MaxLength { get; set; }

        /// <summary>
        /// When set to **true**, sets this as a payment tab. Can only be used with Text, Number, Formula, or List tabs. The value of the tab must be a number.
        /// </summary>
        /// <value>When set to **true**, sets this as a payment tab. Can only be used with Text, Number, Formula, or List tabs. The value of the tab must be a number.</value>
        [JsonProperty(PropertyName = "isPaymentAmount")]
        public string IsPaymentAmount { get; set; }


        /// <summary>
        /// The Formula string contains the TabLabel for the reference tabs used in the formula and calculation operators. Each TabLabel must be contained in brackets. \nMaximum Length: 2000 characters.\n\n*Example*: Three tabs (TabLabels: Line1, Line2, and Tax) need to be added together. The formula string would be: \n\n[Line1]+[Line2]+[Tax]
        /// </summary>
        /// <value>The Formula string contains the TabLabel for the reference tabs used in the formula and calculation operators. Each TabLabel must be contained in brackets. \nMaximum Length: 2000 characters.\n\n*Example*: Three tabs (TabLabels: Line1, Line2, and Tax) need to be added together. The formula string would be: \n\n[Line1]+[Line2]+[Tax]</value>
        [JsonProperty(PropertyName = "formula")]
        public string Formula { get; set; }


        /// <summary>
        /// A regular expressionn used to validate input for the tab.
        /// </summary>
        /// <value>A regular expressionn used to validate input for the tab.</value>
        [JsonProperty(PropertyName = "validationPattern")]
        public string ValidationPattern { get; set; }


        /// <summary>
        /// The message displayed if the custom tab fails input validation (either custom of embedded).
        /// </summary>
        /// <value>The message displayed if the custom tab fails input validation (either custom of embedded).</value>
        [JsonProperty(PropertyName = "validationMessage")]
        public string ValidationMessage { get; set; }

        /// <summary>
        /// When set to **true** and shared is true, information must be entered in this field to complete the envelope.
        /// </summary>
        /// <value>When set to **true** and shared is true, information must be entered in this field to complete the envelope.</value>
        [JsonProperty(PropertyName = "requireAll")]
        public string RequireAll { get; set; }


        /// <summary>
        /// Specifies the tool tip text for the tab.
        /// </summary>
        /// <value>Specifies the tool tip text for the tab.</value>
        [JsonProperty(PropertyName="name")]
        public string Name { get; set; }


        /// <summary>
        /// Specifies the value of the tab.
        /// </summary>
        /// <value>Specifies the value of the tab.</value>
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }


        /// <summary>
        /// The initial value of the tab when it was sent to the recipient.
        /// </summary>
        /// <value>The initial value of the tab when it was sent to the recipient.</value>
        [JsonProperty(PropertyName = "originalValue")]
        public string OriginalValue { get; set; }
        /// <summary>
        /// When set to **true**, the signer is required to fill out this tab
        /// </summary>
        /// <value>When set to **true**, the signer is required to fill out this tab</value>
        [JsonProperty(PropertyName = "required")]
        public string Required { get; set; }


        /// <summary>
        /// When set to **true**, the signer cannot change the data of the custom tab.
        /// </summary>
        /// <value>When set to **true**, the signer cannot change the data of the custom tab.</value>
        [JsonProperty(PropertyName="locked")]
        public string Locked { get; set; }


        /// <summary>
        /// When set to **true**, the field appears normally while the recipient is adding or modifying the information in the field, but the data is not visible (the characters are hidden by asterisks) to any other signer or the sender.\n\nWhen an envelope is completed the information is available to the sender through the Form Data link in the DocuSign Console.\n\nThis setting applies only to text boxes and does not affect list boxes, radio buttons, or check boxes.
        /// </summary>
        /// <value>When set to **true**, the field appears normally while the recipient is adding or modifying the information in the field, but the data is not visible (the characters are hidden by asterisks) to any other signer or the sender.\n\nWhen an envelope is completed the information is available to the sender through the Form Data link in the DocuSign Console.\n\nThis setting applies only to text boxes and does not affect list boxes, radio buttons, or check boxes.</value>
        [JsonProperty(PropertyName = "concealValueOnDocument")]
        public string ConcealValueOnDocument { get; set; }
        [JsonProperty(PropertyName = "disableAutoSize")]
        public string DisableAutoSize { get; set; }     
        [JsonProperty(PropertyName="tabLabel")]
        public string TabLabel { get; set; }
        [JsonProperty(PropertyName = "font")]
        public string Font { get; set; }
        [JsonProperty(PropertyName = "bold")]
        public string Bold { get; set; }
        [JsonProperty(PropertyName = "italic")]
        public string Italic { get; set; }
        [JsonProperty(PropertyName = "underline")]
        public string Underline { get; set; }


        /// <summary>
        /// The font color used for the information in the tab.\n\nPossible values are: Black, BrightBlue, BrightRed, DarkGreen, DarkRed, Gold, Green, NavyBlue, Purple, or White.
        /// </summary>
        /// <value>The font color used for the information in the tab.\n\nPossible values are: Black, BrightBlue, BrightRed, DarkGreen, DarkRed, Gold, Green, NavyBlue, Purple, or White.</value>
        //[JsonProperty(PropertyName="fontColor")]
        //public string FontColor { get; set; }


        ///// <summary>
        ///// The font size used for the information in the tab.\n\nPossible values are: Size7, Size8, Size9, Size10, Size11, Size12, Size14, Size16, Size18, Size20, Size22, Size24, Size26, Size28, Size36, Size48, or Size72.
        ///// </summary>
        ///// <value>The font size used for the information in the tab.\n\nPossible values are: Size7, Size8, Size9, Size10, Size11, Size12, Size14, Size16, Size18, Size20, Size22, Size24, Size26, Size28, Size36, Size48, or Size72.</value>
        //[JsonProperty(PropertyName="fontSize")]
        //public string FontSize { get; set; }


        /// <summary>
        /// Specifies the document ID number that the tab is placed on. This must refer to an existing Document's ID attribute.
        /// </summary>
        /// <value>Specifies the document ID number that the tab is placed on. This must refer to an existing Document's ID attribute.</value>
        [JsonProperty(PropertyName="documentId")]
        public string DocumentId { get; set; }
  
        
        /// <summary>
        /// Unique for the recipient. It is used by the tab element to indicate which recipient is to sign the Document.
        /// </summary>
        /// <value>Unique for the recipient. It is used by the tab element to indicate which recipient is to sign the Document.</value>
        [JsonProperty(PropertyName="recipientId")]
        public string RecipientId { get; set; }
  
        
        /// <summary>
        /// Specifies the page number on which the tab is located.
        /// </summary>
        /// <value>Specifies the page number on which the tab is located.</value>
        [JsonProperty(PropertyName="pageNumber")]
        public string PageNumber { get; set; }
  
        
        /// <summary>
        /// This indicates the horizontal offset of the object on the page. DocuSign uses 72 DPI when determining position.
        /// </summary>
        /// <value>This indicates the horizontal offset of the object on the page. DocuSign uses 72 DPI when determining position.</value>
        [JsonProperty(PropertyName="xPosition")]
        public string XPosition { get; set; }
  
        
        /// <summary>
        /// This indicates the vertical offset of the object on the page. DocuSign uses 72 DPI when determining position.
        /// </summary>
        /// <value>This indicates the vertical offset of the object on the page. DocuSign uses 72 DPI when determining position.</value>
        [JsonProperty(PropertyName="yPosition")]
        public string YPosition { get; set; }
  
        
        /// <summary>
        /// Anchor text information for a radio button.
        /// </summary>
        /// <value>Anchor text information for a radio button.</value>
        [JsonProperty(PropertyName="anchorString")]
        public string AnchorString { get; set; }
  
        
        /// <summary>
        /// Specifies the X axis location of the tab, in achorUnits, relative to the anchorString.
        /// </summary>
        /// <value>Specifies the X axis location of the tab, in achorUnits, relative to the anchorString.</value>
        [JsonProperty(PropertyName="anchorXOffset")]
        public string AnchorXOffset { get; set; }
  
        
        /// <summary>
        /// Specifies the Y axis location of the tab, in achorUnits, relative to the anchorString.
        /// </summary>
        /// <value>Specifies the Y axis location of the tab, in achorUnits, relative to the anchorString.</value>
        [JsonProperty(PropertyName="anchorYOffset")]
        public string AnchorYOffset { get; set; }
  
        
        /// <summary>
        /// Specifies units of the X and Y offset. Units could be pixels, millimeters, centimeters, or inches.
        /// </summary>
        /// <value>Specifies units of the X and Y offset. Units could be pixels, millimeters, centimeters, or inches.</value>
        [JsonProperty(PropertyName="anchorUnits")]
        public string AnchorUnits { get; set; }
  
        
        /// <summary>
        /// When set to **true**, this tab is ignored if anchorString is not found in the document.
        /// </summary>
        /// <value>When set to **true**, this tab is ignored if anchorString is not found in the document.</value>
        [JsonProperty(PropertyName="anchorIgnoreIfNotPresent")]
        public string AnchorIgnoreIfNotPresent { get; set; }
  
        
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName="anchorCaseSensitive")]
        public string AnchorCaseSensitive { get; set; }
  
        
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName="anchorMatchWholeWord")]
        public string AnchorMatchWholeWord { get; set; }
  
        
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName="anchorHorizontalAlignment")]
        public string AnchorHorizontalAlignment { get; set; }
  
        
        /// <summary>
        /// The unique identifier for the tab. The tabid can be retrieved with the [ML:GET call].
        /// </summary>
        /// <value>The unique identifier for the tab. The tabid can be retrieved with the [ML:GET call].</value>
        [JsonProperty(PropertyName="tabId")]
        public string TabId { get; set; }


        /// <summary>
        /// For conditional fields this is the TabLabel of the parent tab that controls this tab's visibility.
        /// </summary>
        /// <value>For conditional fields this is the TabLabel of the parent tab that controls this tab's visibility.</value>
        [JsonProperty(PropertyName = "conditionalParentLabel")]
        public string ConditionalParentLabel { get; set; }


        /// <summary>
        /// For conditional fields, this is the value of the parent tab that controls the tab's visibility.\n\nIf the parent tab is a Checkbox, Radio button, Optional Signature, or Optional Initial use \"on\" as the value to show that the parent tab is active.
        /// </summary>
        /// <value>For conditional fields, this is the value of the parent tab that controls the tab's visibility.\n\nIf the parent tab is a Checkbox, Radio button, Optional Signature, or Optional Initial use \"on\" as the value to show that the parent tab is active.</value>
        [JsonProperty(PropertyName = "conditionalParentValue")]
        public string ConditionalParentValue { get; set; }

        /// <summary>
        /// Gets or Sets MergeField
        /// </summary>
        [JsonProperty(PropertyName = "mergeField")]
        public MergeField MergeField { get; set; }


        /// <summary>
        /// Indicates the envelope status. Valid values are:\n\n* sent - The envelope is sent to the recipients. \n* created - The envelope is saved as a draft and can be modified and sent later.
        /// </summary>
        /// <value>Indicates the envelope status. Valid values are:\n\n* sent - The envelope is sent to the recipients. \n* created - The envelope is saved as a draft and can be modified and sent later.</value>
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }


        /// <summary>
        /// Gets or Sets ErrorDetails
        /// </summary>
        [JsonProperty(PropertyName="errorDetails")]
        public ErrorDetails ErrorDetails { get; set; }  

    }
}
