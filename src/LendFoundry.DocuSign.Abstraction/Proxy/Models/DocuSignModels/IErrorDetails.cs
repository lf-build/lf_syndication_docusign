﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public interface IErrorDetails
    {
        string ErrorCode { get; set; }
        string Message { get; set; }
    }
}
