﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public interface INotification
    {
        string UseAccountDefaults { get; set; }
        Reminders Reminders { get; set; }
        Expirations Expirations { get; set; }
    }
}
