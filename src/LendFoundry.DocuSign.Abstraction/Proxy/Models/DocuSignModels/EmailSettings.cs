using System.Collections.Generic;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public class EmailSettings :IEmailSettings
    {
      
        [JsonProperty(PropertyName="replyEmailAddressOverride")]
        public string ReplyEmailAddressOverride { get; set; }

        [JsonProperty(PropertyName="replyEmailNameOverride")]
        public string ReplyEmailNameOverride { get; set; }

        [JsonProperty(PropertyName="bccEmailAddresses")]
        public List<BccEmailAddress> BccEmailAddresses { get; set; }

    }
}
