﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public interface IReminders
    {
        string ReminderEnabled { get; set; }

        string ReminderDelay { get; set; }

        string ReminderFrequency { get; set; }
    }
}
