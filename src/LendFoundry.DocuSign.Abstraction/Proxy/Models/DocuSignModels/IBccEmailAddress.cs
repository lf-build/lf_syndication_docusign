﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public interface IBccEmailAddress
    {
        string BccEmailAddressId { get; set; }
        string Email { get; set; }
    }
}
