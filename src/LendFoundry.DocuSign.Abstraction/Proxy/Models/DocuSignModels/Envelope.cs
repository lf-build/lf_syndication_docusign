using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public class Envelope : IEnvelope
    {

        [JsonProperty(PropertyName = "transactionId")]
        public string TransactionId { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "documentsUri")]
        public string DocumentsUri { get; set; }

        [JsonProperty(PropertyName = "recipientsUri")]
        public string RecipientsUri { get; set; }

        [JsonProperty(PropertyName = "asynchronous")]
        public string Asynchronous { get; set; }

        [JsonProperty(PropertyName = "envelopeUri")]
        public string EnvelopeUri { get; set; }

        [JsonProperty(PropertyName = "emailSubject")]
        public string EmailSubject { get; set; }

        [JsonProperty(PropertyName = "emailBlurb")]
        public string EmailBlurb { get; set; }

        [JsonProperty(PropertyName = "envelopeId")]
        public string EnvelopeId { get; set; }

        [JsonProperty(PropertyName = "signingLocation")]
        public string SigningLocation { get; set; }

        [JsonProperty(PropertyName = "customFieldsUri")]
        public string CustomFieldsUri { get; set; }

        [JsonProperty(PropertyName = "envelopeIdStamping")]
        public string EnvelopeIdStamping { get; set; }

        [JsonProperty(PropertyName = "authoritativeCopy")]
        public string AuthoritativeCopy { get; set; }

        [JsonProperty(PropertyName = "notificationUri")]
        public string NotificationUri { get; set; }

        [JsonProperty(PropertyName = "enforceSignerVisibility")]
        public string EnforceSignerVisibility { get; set; }

        [JsonProperty(PropertyName = "enableWetSign")]
        public string EnableWetSign { get; set; }

        [JsonProperty(PropertyName = "allowMarkup")]
        public string AllowMarkup { get; set; }

        [JsonProperty(PropertyName = "allowReassign")]
        public string AllowReassign { get; set; }

        [JsonProperty(PropertyName = "createdDateTime")]
        public string CreatedDateTime { get; set; }

        [JsonProperty(PropertyName = "lastModifiedDateTime")]
        public string LastModifiedDateTime { get; set; }

        [JsonProperty(PropertyName = "deliveredDateTime")]
        public string DeliveredDateTime { get; set; }

        [JsonProperty(PropertyName = "sentDateTime")]
        public string SentDateTime { get; set; }

        [JsonProperty(PropertyName = "completedDateTime")]
        public string CompletedDateTime { get; set; }

        [JsonProperty(PropertyName = "voidedDateTime")]
        public string VoidedDateTime { get; set; }

        [JsonProperty(PropertyName = "voidedReason")]
        public string VoidedReason { get; set; }

        [JsonProperty(PropertyName = "deletedDateTime")]
        public string DeletedDateTime { get; set; }

        [JsonProperty(PropertyName = "declinedDateTime")]
        public string DeclinedDateTime { get; set; }

        [JsonProperty(PropertyName = "statusChangedDateTime")]
        public string StatusChangedDateTime { get; set; }

        [JsonProperty(PropertyName = "documentsCombinedUri")]
        public string DocumentsCombinedUri { get; set; }

        [JsonProperty(PropertyName = "certificateUri")]
        public string CertificateUri { get; set; }

        [JsonProperty(PropertyName = "templatesUri")]
        public string TemplatesUri { get; set; }

        [JsonProperty(PropertyName = "messageLock")]
        public string MessageLock { get; set; }

        [JsonProperty(PropertyName = "recipientsLock")]
        public string RecipientsLock { get; set; }

        [JsonProperty(PropertyName = "useDisclosure")]
        public string UseDisclosure { get; set; }

        [JsonProperty(PropertyName = "notification")]
        public Notification Notification { get; set; }

        [JsonProperty(PropertyName = "emailSettings")]
        public EmailSettings EmailSettings { get; set; }

        [JsonProperty(PropertyName = "purgeState")]
        public string PurgeState { get; set; }

        [JsonProperty(PropertyName = "lockInformation")]
        public LockInformation LockInformation { get; set; }

        [JsonProperty(PropertyName = "is21CFRPart11")]
        public string Is21CFRPart11 { get; set; }

    }
}
