using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public class EnvelopeSummary : ErrorDetails, IEnvelopeSummary
    {
        [JsonProperty(PropertyName = "envelopeId")]
        public string EnvelopeId { get; set; }

        [JsonProperty(PropertyName = "uri")]
        public string Uri { get; set; }

        [JsonProperty(PropertyName = "statusDateTime")]
        public string StatusDateTime { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }

}

