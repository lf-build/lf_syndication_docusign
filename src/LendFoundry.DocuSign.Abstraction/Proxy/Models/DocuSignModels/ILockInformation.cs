﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public interface ILockInformation
    {
        UserInfo LockedByUser { get; set; }
        string LockedByApp { get; set; }
        string LockedUntilDateTime { get; set; }
        string LockDurationInSeconds { get; set; }
        string LockType { get; set; }
        string UseScratchPad { get; set; }
        string LockToken { get; set; }
        ErrorDetails ErrorDetails { get; set; }
    }
}
