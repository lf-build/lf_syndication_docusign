using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public class BccEmailAddress : IBccEmailAddress
    {
        [JsonProperty(PropertyName = "bccEmailAddressId")]
        public string BccEmailAddressId { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
    }
}
