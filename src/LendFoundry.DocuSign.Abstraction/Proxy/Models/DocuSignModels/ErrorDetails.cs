using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public class ErrorDetails :IErrorDetails
    {
        [JsonProperty(PropertyName="errorCode")]
        public string ErrorCode { get; set; }

        [JsonProperty(PropertyName="message")]
        public string Message { get; set; }
    }
}
