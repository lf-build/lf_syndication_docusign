using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public class Notification : INotification
    {
        [JsonProperty(PropertyName="useAccountDefaults")]
        public string UseAccountDefaults { get; set; }

        [JsonProperty(PropertyName="reminders")]
        public Reminders Reminders { get; set; }

        [JsonProperty(PropertyName="expirations")]
        public Expirations Expirations { get; set; }
    }
}
