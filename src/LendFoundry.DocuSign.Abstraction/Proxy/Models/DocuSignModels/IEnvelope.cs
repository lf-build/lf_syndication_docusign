﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public interface IEnvelope
    {

        string TransactionId { get; set; }
        string Status { get; set; }
        string DocumentsUri { get; set; }
        string RecipientsUri { get; set; }
        string Asynchronous { get; set; }
        string EnvelopeUri { get; set; }
        string EmailSubject { get; set; }
        string EmailBlurb { get; set; }
        string EnvelopeId { get; set; }
        string SigningLocation { get; set; }
        string CustomFieldsUri { get; set; }
        string EnvelopeIdStamping { get; set; }
        string AuthoritativeCopy { get; set; }
        string NotificationUri { get; set; }
        string EnforceSignerVisibility { get; set; }
        string EnableWetSign { get; set; }
        string AllowMarkup { get; set; }
        string AllowReassign { get; set; }
        string CreatedDateTime { get; set; }
        string LastModifiedDateTime { get; set; }
        string DeliveredDateTime { get; set; }
        string SentDateTime { get; set; }
        string CompletedDateTime { get; set; }
        string VoidedDateTime { get; set; }
        string VoidedReason { get; set; }
        string DeletedDateTime { get; set; }
        string DeclinedDateTime { get; set; }
        string StatusChangedDateTime { get; set; }
        string DocumentsCombinedUri { get; set; }
        string CertificateUri { get; set; }
        string TemplatesUri { get; set; }
        string MessageLock { get; set; }
        string RecipientsLock { get; set; }
        string UseDisclosure { get; set; }
        Notification Notification { get; set; }
        EmailSettings EmailSettings { get; set; }
        string PurgeState { get; set; }
        LockInformation LockInformation { get; set; }
        string Is21CFRPart11 { get; set; }
    }
}
