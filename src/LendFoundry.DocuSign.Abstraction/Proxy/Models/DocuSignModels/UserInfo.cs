using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{

    public class UserInfo : IUserInfo
    {
        [JsonProperty(PropertyName="userName")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName="email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName="userId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName="userType")]
        public string UserType { get; set; }

        [JsonProperty(PropertyName="userStatus")]
        public string UserStatus { get; set; }

        [JsonProperty(PropertyName="uri")]
        public string Uri { get; set; }

        [JsonProperty(PropertyName="errorDetails")]
        public ErrorDetails ErrorDetails { get; set; }

    }
}
