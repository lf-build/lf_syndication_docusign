﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public interface IUserInfo
    {
        string UserName { get; set; }
        string Email { get; set; }
        string UserId { get; set; }
        string UserType { get; set; }
        string UserStatus { get; set; }
        string Uri { get; set; }
        ErrorDetails ErrorDetails { get; set; }
    }
}
