﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels
{
    public interface IEmailSettings
    {
        string ReplyEmailAddressOverride { get; set; }
        string ReplyEmailNameOverride { get; set; }

        List<BccEmailAddress> BccEmailAddresses { get; set; }
    }
}
