﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models
{
    public interface IDocumentFile
    {
        byte[] FileBtyes { get; set; }
        string DocumentId { get; set; }
        string DocumentName { get; set; }
    }
}
