﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models {
    public interface IReceipient {
        string Name { get; set; }
        string Email { get; set; }
        string ClientId { get; set; }
        int RoutingNumber { get; set; }
        string RecipientId { get; set; }
    }
}