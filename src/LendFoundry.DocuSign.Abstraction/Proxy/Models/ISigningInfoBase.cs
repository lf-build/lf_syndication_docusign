﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Models
{
    public interface ISigningInfoBase
    {
        string PageNumber { get; set; }
        string XPosition { get; set; }
        string YPosition { get; set; }
    }
}
