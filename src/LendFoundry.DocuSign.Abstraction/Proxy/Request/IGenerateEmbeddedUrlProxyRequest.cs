﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Request
{
    public interface IGenerateEmbeddedUrlProxyRequest
    {
        string SignerEmail { get; set; }
        string ClientUserId { get; set; }
        string EnvelopeId { get; set; }
        string ReturnUrl { get; set; }
        string SignerName { get; set; }
        string RecipientId { get; set; }
    }
}
