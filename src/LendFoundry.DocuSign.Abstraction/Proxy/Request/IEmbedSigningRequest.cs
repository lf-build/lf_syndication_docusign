﻿using LendFoundry.Syndication.LFDocuSign.Proxy.Models;
using LendFoundry.Syndication.LFDocuSign.TemplateConfiguration;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Request
{
    public interface IEmbedSigningRequest
    {
        string ClientId { get; set; }
        IList<IDocumentFile> File { get; set; }
        IList<IReceipient> Receipients { get; set; }
        IList<IReceipient> CCReceipients { get; set; }
        IDocuSignTemplate TemplateDocuSignConfig { get; set; }
        string EmailBody { get; set; }
    }
}
