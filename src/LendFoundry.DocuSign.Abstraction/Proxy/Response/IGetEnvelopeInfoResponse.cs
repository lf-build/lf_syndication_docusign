﻿using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Response
{
    public interface IGetEnvelopeInfoResponse
    {
        Envelope Envelope { get; set; }
    }
}
