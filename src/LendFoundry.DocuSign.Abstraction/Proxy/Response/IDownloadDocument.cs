﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Response
{
    public interface IDownloadDocument
    {
        string FileName { get; set; }
        string Data { get; set; }
    }
}
