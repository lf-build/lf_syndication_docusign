﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Response
{
    public class DownloadDocument:IDownloadDocument
    {
        public string FileName { get; set; }
        public string Data { get; set; }
    }
}
