﻿
using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;

namespace LendFoundry.Syndication.LFDocuSign.Proxy.Response
{
    public interface IGenerateEmbeddedViewResponse
    {
        IEnvelopeSummary EnvelopeSummary { get; set; }
        string[] RecepientIds { get; set; }
    }
}
