﻿namespace LendFoundry.Syndication.LFDocuSign.Proxy.Response
{
    public interface IGenerateEmbeddedUrlResponse
    {
        string Url { get; set; }
    }
}
