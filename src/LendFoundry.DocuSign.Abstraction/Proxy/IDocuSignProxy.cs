﻿using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;
using LendFoundry.Syndication.LFDocuSign.Proxy.Request;
using LendFoundry.Syndication.LFDocuSign.Proxy.Response;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.Proxy
{
    public interface IDocuSignProxy
    {
        IGenerateEmbeddedViewResponse GenerateEmbeddedView(string entityId, IEmbedSigningRequest embedSigningRequest, string documentType);
        IGenerateEmbeddedUrlResponse GenerateEmbeddedUrl(IGenerateEmbeddedUrlProxyRequest request);
        bool HasAuthenticated(string envelopeId);
        EnvelopeAuditEventResponse GetAuthenticationInfo(string envelopeId);
        IGetEnvelopeInfoResponse GetEnvelopeInfo(string envelopeId);
        List<DownloadDocument> DownloadDocuments(string envelopeId);
        dynamic GetReceipientData(string envelopeId, string receipientId);
    }
}
