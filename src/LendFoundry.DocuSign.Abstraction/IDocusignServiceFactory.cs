﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
namespace LendFoundry.Syndication.LFDocuSign
{
    public interface IDocusignServiceFactory
    {
        IDocuSignService Create(ITokenReader reader, ILogger logger);
    }
}
