﻿namespace LendFoundry.Syndication.LFDocuSign.Response
{
    public interface IDownloadDocument
    {
        string FileName { get; set; }
        string Data { get; set; }
    }
}
