﻿namespace LendFoundry.Syndication.LFDocuSign.Response
{
    public class DownloadDocument:IDownloadDocument
    {
        public string FileName { get; set; }
        public string Data { get; set; }
    }
}
