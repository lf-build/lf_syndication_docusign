﻿using LendFoundry.Syndication.LFDocuSign.Models;

namespace LendFoundry.Syndication.LFDocuSign.Response
{
    public interface IGetEnvelopeInfoResponse
    {
        IEnvelope Envelope { get; set; }
    }
}
