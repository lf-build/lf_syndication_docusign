﻿namespace LendFoundry.Syndication.LFDocuSign.Response
{
    public interface IEmbeddedUrlResponse
    {
        string Url { get; set; }
    }
}
