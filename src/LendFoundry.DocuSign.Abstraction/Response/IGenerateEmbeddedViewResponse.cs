﻿using LendFoundry.Syndication.LFDocuSign.Models;

namespace LendFoundry.Syndication.LFDocuSign.Response
{
    public interface IGenerateEmbeddedViewResponse
    {
        IEnvelopeSummary EnvelopeSummary { get; set; }
        string []RecepientIds { get; set; }
    }
}
