
using LendFoundry.Foundation.Date;
namespace LendFoundry.Syndication.LFDocuSign.Request {
  
    public interface IReceipientStatus {
        string Name { get; set; }
        string Email { get; set; }
        string RecipientId { get; set; }
        string Status { get; set; }
        string SignedOn { get; set; }
    }
}