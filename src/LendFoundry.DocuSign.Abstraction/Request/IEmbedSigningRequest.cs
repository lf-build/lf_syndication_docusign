﻿using LendFoundry.Syndication.LFDocuSign.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.Request
{
    public interface IEmbedSigningRequest
    {
        List<string> DocumentId { get; set; }
        string ClientId { get; set; }
        IList<IReceipient> Receipients { get; set; }
        IList<IReceipient> CCReceipients { get; set; }
        string EmailBody { get; set; }
    }
}
