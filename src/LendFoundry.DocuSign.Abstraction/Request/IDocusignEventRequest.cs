﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.Request {
    public interface IDocusignEventRequest {
        List<string> SignerEmail { get; set; }
        List<string> DocumentBytes { get; set; }
        string Status { get; set; }
        string EntityId { get; set; }
        string EntityType { get; set; }
        List<IReceipientStatus> ReceipientStatus { get; set; }
    }
}