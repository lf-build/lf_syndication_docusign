﻿namespace LendFoundry.Syndication.LFDocuSign.Request
{
    public interface IGenerateEmbeddedUrlRequest
    {
        string SignerEmail { get; set; }
        string ClientUserId { get; set; }
        string EnvelopeId { get; set; }
        string SignerName { get; set; }
        string ReturnUrl { get; set; }  
        string RecipientId { get; set; }
    }
}
