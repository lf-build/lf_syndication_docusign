﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Syndication.LFDocuSign
{
    /// <summary>
    /// 
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "docusign";
    }
}