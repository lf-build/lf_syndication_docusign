﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Syndication.LFDocuSign.Proxy.Models.DocuSignModels;
using LendFoundry.Syndication.LFDocuSign.TemplateConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LFDocuSign
{
    public interface IEnvelopeInfo : IAggregate
    {
        string EntityId { get; set; }
        List<string> Documents { get; set; }
        string EnvelopeId { get; set; }
        List<ReceipientInfo> Signers { get; set; }
        TimeBucket Time { get; set; }
    }
}
