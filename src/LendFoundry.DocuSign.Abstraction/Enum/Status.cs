namespace LendFoundry.Syndication.LFDocuSign {

    public enum Status {
        None = 0,
        Sent = 1,
        Completed = 2

    }
}