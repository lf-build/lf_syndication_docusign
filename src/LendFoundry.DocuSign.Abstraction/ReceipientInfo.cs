﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LFDocuSign
{
    public class ReceipientInfo
    {
        public string ReceipientId { get; set; }
        public string RoutingNumber { get; set; }
        public string Name { get; set; }
    }
}
