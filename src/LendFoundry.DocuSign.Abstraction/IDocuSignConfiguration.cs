﻿using LendFoundry.Syndication.LFDocuSign.TemplateConfiguration;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace LendFoundry.Syndication.LFDocuSign
{
    public interface IDocuSignConfiguration: IDependencyConfiguration
    {
        string UserName { get; set; }
        string Password { get; set; }
        string IntegratorKey { get; set; }
        string Url { get; set; }
        List<IDocuSignTemplate> DocumentSignature { get; set; }
        List<EventConfiguration> events { get; set; }
        string ConnectionString { get; set; }
    }
}
