﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public interface IDocuSignTab
    {
        TabTypes TabType { get; set; }
        ITabConfiguration Configuration { get; set; }
    }
}
