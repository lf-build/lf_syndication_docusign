﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public interface ISignerConfiguration
    {
        int? RoutingOrder { get; set; }
        string RecipientId { get; set; }
        IList<IDocuSignTab> Tabs { get; set; }
    }
}
