﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public interface IRecipientEvent
    {
        DocusignEventsType EventType { get; set; }
        bool GetDocumetOnEvent { get; set; }
    }
}
