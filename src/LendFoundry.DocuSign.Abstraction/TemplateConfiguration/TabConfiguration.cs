﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public class TabConfiguration: ITabConfiguration
    {
        public bool AnchorCaseSensitive { get; set; }
        public bool AnchorMatchWholeWord { get; set; }
        public string AnchorString { get; set; }
        public long AnchorXOffset { get; set; }
        public long AnchorYOffset { get; set; }
        public string Name { get; set; }
        public int PageNumber { get; set; }
        public int TabOrder { get; set; }
        public long XPosition { get; set; }
        public long YPosition { get; set; }
        public bool Required { get; set; }
    }
}
