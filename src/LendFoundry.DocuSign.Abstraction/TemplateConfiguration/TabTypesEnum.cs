﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public enum TabTypes : int
    {
        DateSigned,
        Decline,
        FirstName,
        InitialHere,
        LastName,
        SignHere,
        TextTab,
        SsnTab,
        ZipTab,
        EmailTab,
    }
}
