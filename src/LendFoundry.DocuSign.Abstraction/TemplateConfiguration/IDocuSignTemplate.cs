﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public interface IDocuSignTemplate
    {
        string ReturnUrl { get; set; }
        string BrandId { get; set; }
        string DocumentName { get; set; }
        string EmailSubject { get; set; }
        IDocuSignEventConfigurationsSettings EventConfigurations { get; set; }
        IExpirationSettings ExpirationSetting { get; set; }
        IReminderSettings ReminderSetting { get; set; }
        IList<ISignerConfiguration> SignerConfiguration { get; set; }
    }
}
