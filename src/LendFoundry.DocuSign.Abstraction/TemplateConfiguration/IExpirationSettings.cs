﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public interface IExpirationSettings
    {
        int ExpireAfterDays { get; set; }
        int WarningExpireBeforeDays { get; set; }
    }
}
