﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public interface IDocuSignEventConfigurationsSettings
    {
        string ListnerUrl { get; set; }
        bool GetDocumentFields { get; set; }
        bool GetDocuments { get; set; }
        bool GetRejectionReason { get; set; }
        bool EnableLogging { get; set; }
        IList<IRecipientEvent> RecipientEvent { get; set; }
    }
}
