﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public enum DocusignEventsType : int
    {
        Sent,
        Delivered,
        Completed,
        Declined,
        AuthenticationFailed,
        AutoResponded
    }
}
