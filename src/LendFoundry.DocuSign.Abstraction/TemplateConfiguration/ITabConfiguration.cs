﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public interface ITabConfiguration
    {
        bool AnchorCaseSensitive { get; set; }
        bool AnchorMatchWholeWord { get; set; }
        string AnchorString { get; set; }
        long AnchorXOffset { get; set; }
        long AnchorYOffset { get; set; }
        string Name { get; set; }
        int PageNumber { get; set; }
        int TabOrder { get; set; }
        long XPosition { get; set; }
        long YPosition { get; set; }
        bool Required { get; set; }
    }
}
