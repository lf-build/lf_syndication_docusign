﻿namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public interface IReminderSettings
    {
        int FirstReminderAfterDays { get; set; }
        int ReminderInterval { get; set; }
    }
}
