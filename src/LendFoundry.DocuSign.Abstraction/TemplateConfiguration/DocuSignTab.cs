﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.LFDocuSign.TemplateConfiguration
{
    public class DocuSignTab : IDocuSignTab
    {
        public TabTypes TabType { get; set; }
        [JsonConverter(typeof(ConcreteJsonConverter<TabConfiguration>))]
        public ITabConfiguration Configuration { get; set; }
    }
}
