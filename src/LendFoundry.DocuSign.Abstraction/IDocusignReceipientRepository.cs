﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Syndication.LFDocuSign.Models;

namespace LendFoundry.Syndication.LFDocuSign {
    public interface IDocusignReceipientRepository : IRepository<IReceipientDetails> {
        Task AddReceipientDetails (IReceipientDetails receipientDetails);
        Task<IReceipientDetails> UpdateReceipientDetails (IReceipientDetails receipientDetails);
        Task<IReceipientDetails> UpdateReceipientStatus (string entityId, string recipientId, Status status);
        Task<List<IReceipientDetails>> GetReceipientByEntityId (string entityId);
    }
}